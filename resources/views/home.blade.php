
@extends('layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/home.css') }}">
@endsection

@section('content')

    <section class="yourFinance container-fluid">
        <div class="container">
            <div class="row">
                <div class="takeCare col-md-6 col-xs-12">
                    <h2 class="text-center">
                        <a href="#"><img src="{{ asset(PATH_ROOT .'assets/images/custom/komokun_top.png')}}" width="597" height="708" class="img-responsive" alt="" /></a>
                    </h2>
                    <div class="partner">
                        <div>
                            <a href="brand.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/hsbc-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/acb-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/agribank-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/citi-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/dong-a-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/exim-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/maritime-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/MB-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/ocb-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/sacom-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/shinhan.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/standard.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/techcom.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/tpbank-home.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                        <div>
                            <a href="category.html"><img src="{{ asset(PATH_ROOT .'assets/images/logo/vib.png')}}" width="195" height="53" alt="" /></a>
                        </div>
                    </div>
                </div>
                <div class="chatPanel col-md-6 col-xs-12">
                    <main>
                        <div class="custom-home-chat">
                            <img class="img-title-chat" src="{{ asset(PATH_ROOT .'assets/images/custom/icon-title-chat.png')}}">
                            <div class="title-chat">bạn cần mở thẻ tín dụng?<br> chỉ cần cho comokun biết:</div>
                        </div>
                        {!! Form::open(array('id' => "search_results_frm", 'method' => 'POST', 'route' => "category")) !!}
                        <section id="content">
                            <div class="speech-wrapper">
                                <div class="bubble clearfix">
                                    <div class="txt pull-left">
                                        <p class="message">Thu nhập hàng tháng?</p>
                                    </div>
                                    <div class="bubble-arrow"></div>
                                </div>
                                <!--Speech Bubble alternative -->
                                <div class="bubble clearfix alt">
                                    <div class="txt pull-right">
                                        <div class="message icon-drop">
                                            <select name="searchSalary" class="salary" id="salary">
                                                <option value="less5000">Dưới 5,000,000 VND</option>
                                                <option value="5000to10000">5,000,000 đến 10,000,000 VND</option>
                                                <option value="10000to15000">10,000,000 đến 15,000,000 VND</option>
                                                <option value="larger15000">Trên 15,000,000 VND</option>
                                            </select>
                                            <div id="salary-tem">
                                                <option></option>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bubble-arrow alt"></div>
                                </div>
                            </div>
                            <div class="speech-wrapper">
                                <div class="bubble clearfix">
                                    <div class="txt pull-left">
                                        <p class="message">Hình thức nhận lương?</p>
                                    </div>
                                    <div class="bubble-arrow"></div>
                                </div>
                                <!--Speech Bubble alternative -->
                                <div class="bubble clearfix alt">
                                    <div class="txt">
                                        <div class="message icon-drop">
                                            <select name="searchPaymentMethod" class="selectOpt" id="payment">
                                                <option value="0" selected>Chuyển khoản</option>
                                                <option value="1">Tiền mặt</option>
                                            </select>
                                            <div id="payment-temp">
                                                <option></option>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bubble-arrow alt"></div>
                                </div>
                            </div>
                            <div class="speech-wrapper">
                                <div class="bubble clearfix">
                                    <div class="txt pull-left">
                                        <p class="message">Nhu cầu mở thẻ?</p>
                                    </div>
                                    <div class="bubble-arrow"></div>
                                </div>
                                <!--Speech Bubble alternative -->
                                <div class="bubble clearfix alt">
                                    <div class="txt">
                                        <div class="message icon-drop">
                                            <select name="searchAccumulatedIncentive" class="selectOpt" id="action">
                                                <option value="1">Mua sắm và giải trí</option>
                                                <option value="2">Mua sắm trả góp</option>
                                                <option value="3">Công tác, du lịch &amp; tích lũy dặm bay</option>
                                                <option value="4">Hoàn tiền</option>
                                                <option value="5">Trả phí bảo hiểm</option>
                                                <option value="6">Di chuyển hàng ngày</option>
                                            </select>
                                            <div id="action-temp">
                                                <option></option>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bubble-arrow alt"></div>
                                </div>
                            </div>
                        </section>
                        <div class="bgChat">
                            <p class="noteChat">*Bằng việc lựa chọn hiển thị kết quả, bạn đã đồng ý với <br><a href="terms.html" class="text-semibold">Điều Khoản Sử Dụng</a> và <a href="policy.html" class="text-semibold">Chính Sách Bảo Mật</a> của Comokun</p>
                            <button type="submit" class="showResults" >HIỂN THỊ KẾT QUẢ</button>
                            <a href="javascript:void(0)" id="home-refresh" class="refresh-home">
                                <img src="{{ asset(PATH_ROOT .'assets/images/icon/refresh.png')}}"> Lựa chọn lại
                            </a>
                        </div>

                        {!! Form::close() !!}
                    </main>
                </div>
            </div>
        </div>
    </section>

    <!--start Vân-->
    <div class="wrapper">
        <div class="container choiceAward">
            <h2>Lựa chọn của Comokun</h2>
            <div class="award_slide">
                <div class="awardArea row">

                    @if(isset($card_arr) && count($card_arr) > 0)
                        @foreach($card_arr as $card)
                            <div class="award_col_pc" >
                                <div class="award_col" style="width: 270px">
                                    <div class="ttl_awd" style="height: 50px">
                                        <h3 class="icon_adw_01"><?php echo strlen($card->ten_the) <= 33 ? $card->ten_the : substr($card->ten_the, 0, 33)."..." ?></h3>
                                    </div>
                                    <p class="awd_img"><img src="{{ PATH_IMAGE.$card->image }}" alt=""></p>
                                    <ul class="starList cf">
                                        <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd_off.png')}}" alt=""></li>
                                        <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd_off.png')}}" alt=""></li>
                                        <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd_off.png')}}" alt=""></li>
                                        <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd_off.png')}}" alt=""></li>
                                        <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_awd_off.png')}}" alt=""></li>
                                    </ul>
                                    <p class="awd_txt01" style="height: 30px;"><?php echo strlen($card->khuyen_mai_mo_the_text) <= 60 ? $card->khuyen_mai_mo_the_text : substr($card->khuyen_mai_mo_the_text, 0, 60)."..." ?></p>
                                    <p class="awd_txt02">Hạn mức tín dụng: <?php echo isset($card->han_muc_tin_dung)? number_format($card->han_muc_tin_dung) : 0 ?> VND</p>
                                    <p class="awd_txt02">Yêu cầu thu nhập: <?php echo isset($card->yeu_cau_thu_nhap) ? number_format($card->yeu_cau_thu_nhap) : 0 ?> VND</p>
                                    <p class="awd_txt02 mb23">Phí thường niên: <?php echo isset($card->phi_thuong_nien) ? number_format($card->phi_thuong_nien) : 0 ?> VND<br> (Miễn phí năm đầu)</p>
                                    <div class="awd_more"><a class="btn-home-detail" href="javascript:void(0);" data-href="{{ asset("card/".$card->id) }}">Xem thêm</a></div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>


        </div>
        <div class="container awd_block">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adw_block_img"> <img src="{{ asset(PATH_ROOT .'assets/images/adw_block01.jpg')}}" alt=""> </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adw_block_img"> <img src="{{ asset(PATH_ROOT .'assets/images/adw_block02.jpg')}}" alt=""> </div>
            </div>
        </div>
        <div class="container awd_about">
            <div class="row">
                <div class="col-ls-6 col-md-6 col-xs-12 awd_about_left">
                    <div class="ttl_awd_about">
                        <h2>Comokun là ai?</h2>
                    </div>
                    <div class="txt_awd_about">
                        <p>Là chàng báo đen đến từ Nhật bản, rất thông minh và nhanh nhẹn, tên là Como, hay được bạn bè gọi là Comokun. Trong tiếng Nhật, "kun" là cách gọi yêu cho các cậu bé, hoặc chàng trai trẻ. Tên này thuộc bản quyền của công ty Transcosmos
                            - là nhà tài trợ website này, vừa là viết tắt của COmparison MObility - nghĩa là sự So Sánh Linh Động các loại thẻ tín dụng. Comokun sẽ giúp bạn tìm hiểu và lựa chọn được thẻ phù hợp với mình nhất! </p>
                    </div>
                    <div class="see_more"><a href="about_us.html">Xem thêm</a></div>
                </div>
                <div class="col-ls-6 col-md-6 col-xs-12 awd_about_cat"> <img src="{{ asset(PATH_ROOT .'assets/images/awd_about_cat.png')}}" alt=""> </div>
            </div>
        </div>
        <div class="container fb_customer">
            <div class="row">
                <div class="ttl_fb_customer">
                    <h2>Ý kiến truyền thông</h2>
                </div>
                <div class="col-ls-6 col-md-6 col-xs-12 col_fb_customer">
                    <div class="img_fb_cus"> <img src="{{ asset(PATH_ROOT .'assets/images/img_cus01.png')}}" alt=""> </div>
                    <div class="txt_fb_customer">
                        <p class="txt_fb_cus">Tôi đánh giá đây là giải pháp đón đầu xu hướng tiêu dùng của thị trường Việt Nam, không chỉ cung cấp những thông tin hữu ích và công cụ so sánh tối ưu cho các loại thẻ tín dụng trên thị trường mà còn đem đến những ưu đãi hấp dẫn
                            cho người dùng nhằm mục đích gia tăng dân số “người tiêu dùng thông minh”. Một giải pháp tuyệt vời cho những ai không muốn bỏ lại sau trong bối cảnh phát triển số hóa ngày nay.</p>
                        <p class="name_fb_cus">Yohei Koumura</p>
                        <p class="pos_fb_cus">Tổng Giám Đốc <span>@ transcosmos Vietnam Co., Ltd</span></p>
                    </div>
                </div>
                <div class="col-ls-6 col-md-6 col-xs-12 col_fb_customer">
                    <div class="img_fb_cus"> <img src="{{ asset(PATH_ROOT .'assets/images/img_cus02.png')}}" alt=""> </div>
                    <div class="txt_fb_customer">
                        <p class="txt_fb_cus">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        <p class="name_fb_cus">Stewart Butterfield</p>
                        <p class="pos_fb_cus">CTO &amp; Co-Founder <span>@ Facebook.com</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modalDetail" id="popupDetail">
        <div class="modal-dialog wrapDetail">
            <div class="kvDetail">
                <img src="{{ asset('assets/images/bg_category_detail.png') }}" alt="" class="w100p">
                <a href="#" class="btnCloseDetail"><img src="{{ asset('assets/images/icon_close_detail.png') }}" alt=""></a>
            </div>

            <div class="contentDetail cf">
                <div id="cardInfo">
                @include('card_detail')
                </div>

                <div class="relativeCard">
                    <div class="box04">
                        <h2 class="textRelated text-semibold text-uppercase text-center"><span>Sản phẩm tương tự</span></h2>
                        <ul class="relatedSlider cf">
                            @foreach($card_relative_arr as $card)
                                <li class="cardRelated text-center">
                                    <h4 class="cardTitle"><a class="btn-home-detail" href="javascript:void(0);" data-href="{{ asset("card/".$card->id) }}"> {{$card->ten_the}} </a></h4>
                                    <a class="btn-home-detail" href="javascript:void(0);" data-href="{{ asset("card/".$card->id) }}"><img src="{{ PATH_IMAGE.$card->image }}" alt=""></a>
                                </li>
                            @endforeach

                        </ul>
                        <div class="relatedSliderDots"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>



@endsection

@section('script')
    <script>

        /* ==== Start Home Page ==== */
        var slicked = false;
        $('.btn-home-detail').click(function(e) {
            e.preventDefault();

            var dataURL = $(this).attr('data-href');

            // load data from database
            $('#cardInfo').load(dataURL, function(){
                $("[tab].is-active>[tab-body]").slideDown(0);
                $("[tab-header-pc]").on("click", function() {
                    var index = $(this).index(),
                            tablist = $(this).closest("[headerlist]").next(),
                            tabActive = tablist.find("[tab]").eq(index);
                    $(this).siblings().removeClass("is-active");
                    $(this).parent().children().eq(index).addClass("is-active");
                    tablist.find("[tab]").removeClass("is-active");
                    tabActive.addClass("is-active");
                    tabActive.siblings().children("[tab-body]").slideUp(0);
                    tabActive.children("[tab-body]").slideDown(0);

                });
                $("[tab-header-sp]").on("click", function() {
                    var index = $(this).closest("[tab]").index(),
                            tab = $(this).parent();
                    headerActive = $(this).closest("[tab-container]").children("[headerlist]").find("[tab-header-pc]").eq(index);
                    tab.siblings().removeClass("is-active");
                    tab.toggleClass("is-active");
                    tab.siblings().find("[tab-body]").slideUp(500);
                    $(this).next().slideToggle(500);
                    headerActive.siblings().removeClass("is-active");
                    headerActive.toggleClass("is-active");
                });

                $(".tabBody>.inner").mCustomScrollbar({
                    theme: "rounded-dots",
                    mouseWheelPixels: 100,
                    scrollInertia: 0
                });



            });


            // open modal
            $('.modalDetail').addClass('is-active');
            $('body').addClass('modal-open');

            if (!slicked) {
                $('.relatedSlider').slick({
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: true,
                    prevArrow: '<span class="fa fa-angle-left slick-prev"></span>',
                    nextArrow: '<span class="fa fa-angle-right slick-next"></span>',
                    dots: true,
                    appendDots: '.relatedSliderDots',
                    infinite: true,
                    responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                        {
                            breakpoint: 481,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            }
            slicked = true;


        });
        $('.btnCloseDetail').click(function(e) {
            e.preventDefault();
            $('.modalDetail').removeClass('is-active');
            $('body').removeClass('modal-open');
        });
        $(window).click(function() {
            $('.modalDetail').removeClass('is-active');
            $('body').removeClass('modal-open');
        });
        $('.wrapDetail, .btn-home-detail').click(function(e) {
            e.stopPropagation();
        });
        $(window).on('keydown', function(e) {
            var keycode = e.keyCode;
            if (keycode === 27) {
                $('.modalDetail').removeClass('is-active');
                $('body').removeClass('modal-open');
            }
        });

        /* ==== End Home Page ==== */
    </script>
@endsection

