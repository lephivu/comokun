<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/20/16
 * Time: 11:58 PM
 */
?>
@extends('layouts.main_category')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/comparison.css') }}">
@endsection

@section('content')
    <div class="wrapOuter">
        <div class="contentArea">
            <div class="wrapComparison">
                <div class="comparisonInner">
                    <div class="comparisonTop">
                        <div class="comparison_icon"><img src="{{ asset('assets/images/comparison_icon.png') }}" alt=""></div>
                        <ul class="comparison_li">

                            <li class="item_selected">
                                <div class="comparison_content">
                                    @if(isset($card_arr[0]) && $card_arr[0]->feature == ACTIVE)
                                    <div class="iconSpecial cf">
                                        <img src="{{ asset('assets/images/icon_panther_special.png') }}" alt="" class="icon_panther_special">
                                        <p>Lựa chọn của báo đen</p>
                                    </div>
                                    @endif
                                    <div class="insurance_icon"><img src="{{ asset('assets/images/insurance_on.png') }}" alt=""></div>
                                    <div class="close_compaCard"><img src="{{ asset('assets/images/close_compaCard.png') }}" alt=""></div>
                                    <h4 class="compa_title"> <?php echo (isset($card_arr[0]) && !empty($card_arr[0]->ten_the)) ? $card_arr[0]->ten_the : '' ?> </h4>
                                    <div class="compa_card"><img src="<?php echo (isset($card_arr[0]) && !empty($card_arr[0]->image)) ? PATH_IMAGE.$card_arr[0]->image : '' ?>" alt=""></div>
                                    <ul class="starList cf star-3">
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                    </ul>
                                    <div class="regist_card"><a href="javascript:void(0)"  data-toggle="modal" data-target="#login-modal" class="register-button" >Đăng kí ngay</a></div>
                                </div>
                            </li>
                            <li>
                                <div class="comparison_content">
                                    @if(isset($card_arr[1]) && $card_arr[1]->feature == ACTIVE)
                                        <div class="iconSpecial cf">
                                            <img src="{{ asset('assets/images/icon_panther_special.png') }}" alt="" class="icon_panther_special">
                                            <p>Lựa chọn của báo đen</p>
                                        </div>
                                    @endif
                                        <div class="insurance_icon"><img src="{{ asset('assets/images/insurance_on.png') }}" alt=""></div>
                                        <div class="close_compaCard"><img src="{{ asset('assets/images/close_compaCard.png') }}" alt=""></div>
                                        <h4 class="compa_title"> <?php echo (isset($card_arr[1]) && !empty($card_arr[1]->ten_the)) ? $card_arr[1]->ten_the : '' ?> </h4>
                                        <div class="compa_card"><img src="<?php echo (isset($card_arr[1]) && !empty($card_arr[1]->image)) ? PATH_IMAGE.$card_arr[1]->image : '' ?>" alt=""></div>
                                    <ul class="starList cf star-3">
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                    </ul>
                                    <div class="regist_card"><a href="javascript:void(0)"  data-toggle="modal" data-target="#login-modal" class="register-button" >Đăng kí ngay</a></div>
                                </div>
                            </li>
                            <li>
                                <div class="comparison_content">
                                    @if(isset($card_arr[2]) && $card_arr[2]->feature == ACTIVE)
                                        <div class="iconSpecial cf">
                                            <img src="{{ asset('assets/images/icon_panther_special.png') }}" alt="" class="icon_panther_special">
                                            <p>Lựa chọn của báo đen</p>
                                        </div>
                                    @endif
                                        <div class="insurance_icon"><img src="{{ asset('assets/images/insurance_on.png') }}" alt=""></div>
                                        <div class="close_compaCard"><img src="{{ asset('assets/images/close_compaCard.png') }}" alt=""></div>
                                        <h4 class="compa_title"> <?php echo (isset($card_arr[2]) && !empty($card_arr[2]->ten_the)) ? $card_arr[2]->ten_the : '' ?> </h4>
                                        <div class="compa_card"><img src="<?php echo (isset($card_arr[2]) && !empty($card_arr[2]->image)) ? PATH_IMAGE.$card_arr[2]->image : '' ?>" alt=""></div>
                                        <ul class="starList cf star-3">
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        </ul>
                                    <div class="regist_card"><a href="javascript:void(0)"  data-toggle="modal" data-target="#login-modal" class="register-button" >Đăng kí ngay</a></div>
                                </div>
                            </li>
                            <li>
                                <div class="comparison_content">
                                    @if(isset($card_arr[3]) && $card_arr[3]->feature == ACTIVE)
                                        <div class="iconSpecial cf">
                                            <img src="{{ asset('assets/images/icon_panther_special.png') }}" alt="" class="icon_panther_special">
                                            <p>Lựa chọn của báo đen</p>
                                        </div>
                                    @endif
                                        <div class="insurance_icon"><img src="{{ asset('assets/images/insurance_on.png') }}" alt=""></div>
                                        <div class="close_compaCard"><img src="{{ asset('assets/images/close_compaCard.png') }}" alt=""></div>
                                        <h4 class="compa_title"> <?php echo (isset($card_arr[3]) && !empty($card_arr[3]->ten_the)) ? $card_arr[3]->ten_the : '' ?> </h4>
                                        <div class="compa_card"><img src="<?php echo (isset($card_arr[3]) && !empty($card_arr[3]->image)) ? PATH_IMAGE.$card_arr[3]->image : '' ?>" alt=""></div>
                                        <ul class="starList cf star-3">
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png') }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                                        </ul>
                                    <div class="regist_card"><a href="javascript:void(0)"  data-toggle="modal" data-target="#login-modal" class="register-button" >Đăng kí ngay</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="compaCard_detail">
                        <ul class="blocklist">
                            <li>
                                <div class="blockLi_title cf">
                                    <h4>Tính năng nổi bật</h4>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="table_area" style="overflow-x: auto;">
                                    <table class="table_card">
                                        <tr>
                                            <th>Tổ chức thẻ</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->ten_loai) ? $card_arr[0]->ten_loai : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->ten_loai) ? $card_arr[1]->ten_loai : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->ten_loai) ? $card_arr[2]->ten_loai : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->ten_loai) ? $card_arr[3]->ten_loai : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Lãi suất (tháng)</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->lai_suat) ? number_format($card_arr[0]->lai_suat) : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->lai_suat) ? number_format($card_arr[1]->lai_suat) : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->lai_suat) ? number_format($card_arr[2]->lai_suat) : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->lai_suat) ? number_format($card_arr[3]->lai_suat) : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>số ngày ân hạn</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->so_ngay_an_han) ? $card_arr[0]->so_ngay_an_han : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->so_ngay_an_han) ? $card_arr[1]->so_ngay_an_han : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->so_ngay_an_han) ? $card_arr[2]->so_ngay_an_han : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->so_ngay_an_han) ? $card_arr[3]->so_ngay_an_han : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Hạn mức tín dụng</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->han_muc_tin_dung) ? number_format($card_arr[0]->han_muc_tin_dung) : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->han_muc_tin_dung) ? number_format($card_arr[1]->han_muc_tin_dung) : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->han_muc_tin_dung) ? number_format($card_arr[2]->han_muc_tin_dung) : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->han_muc_tin_dung) ? number_format($card_arr[3]->han_muc_tin_dung) : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Hoàn tiền cao nhất</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->hoan_tien_cao_nhat) ? $card_arr[0]->hoan_tien_cao_nhat : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->hoan_tien_cao_nhat) ? $card_arr[1]->hoan_tien_cao_nhat : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->hoan_tien_cao_nhat) ? $card_arr[2]->hoan_tien_cao_nhat : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->hoan_tien_cao_nhat) ? $card_arr[3]->hoan_tien_cao_nhat : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Yêu cầu thu nhập</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->yeu_cau_thu_nhap) ? number_format($card_arr[0]->yeu_cau_thu_nhap) : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->yeu_cau_thu_nhap) ? number_format($card_arr[1]->yeu_cau_thu_nhap) : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->yeu_cau_thu_nhap) ? number_format($card_arr[2]->yeu_cau_thu_nhap) : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->yeu_cau_thu_nhap) ? number_format($card_arr[3]->yeu_cau_thu_nhap) : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>hạn mức rút tiền mặt</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->han_muc_rut_tien_mat) ? number_format($card_arr[0]->han_muc_rut_tien_mat) : '' }}<br><span class="text_Cap table_free">(Miễn phí năm đầu)</span></td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->han_muc_rut_tien_mat) ? number_format($card_arr[1]->han_muc_rut_tien_mat) : '' }}<br><span class="text_Cap table_free">(Miễn phí năm đầu)</span></td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->han_muc_rut_tien_mat) ? number_format($card_arr[2]->han_muc_rut_tien_mat) : '' }}<br><span class="text_Cap table_free">(Miễn phí năm đầu)</span></td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->han_muc_rut_tien_mat) ? number_format($card_arr[3]->han_muc_rut_tien_mat) : '' }}<br><span class="text_Cap table_free">(Miễn phí năm đầu)</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                            <li>
                                <div class="blockLi_title cf">
                                    <h4>các loại phí</h4>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="table_area" style="overflow-x: auto">
                                    <table class="table_card">
                                        <tr>
                                            <th>Phí phát hành</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->phi_phat_hanh) && $card_arr[0]->phi_phat_hanh > 0 ? number_format($card_arr[0]->phi_phat_hanh) : (empty($card_arr[0]->phi_phat_hanh) ? '-' : 'VND') }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->phi_phat_hanh) && $card_arr[1]->phi_phat_hanh > 0 ? number_format($card_arr[1]->phi_phat_hanh) : (empty($card_arr[1]->phi_phat_hanh) ? '-' : 'VND') }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->phi_phat_hanh) && $card_arr[2]->phi_phat_hanh > 0 ? number_format($card_arr[2]->phi_phat_hanh) : (empty($card_arr[2]->phi_phat_hanh) ? '-' : 'VND') }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->phi_phat_hanh) && $card_arr[3]->phi_phat_hanh > 0 ? number_format($card_arr[3]->phi_phat_hanh) : (empty($card_arr[3]->phi_phat_hanh) ? '-' : 'VND') }}</td>
                                        </tr>
                                        <tr>
                                            <th>Phí thường niên</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->phi_thuong_nien) ? number_format($card_arr[0]->phi_thuong_nien) : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->phi_thuong_nien) ? number_format($card_arr[1]->phi_thuong_nien) : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->phi_thuong_nien) ? number_format($card_arr[2]->phi_thuong_nien) : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->phi_thuong_nien) ? number_format($card_arr[3]->phi_thuong_nien) : '' }}</td>
                                        </tr>
                                        <tr class="text-capitalize">
                                            <th>Phí ứng tiền mặt</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->phi_thuong_nien) ? number_format($card_arr[0]->phi_thuong_nien) : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->phi_thuong_nien) ? number_format($card_arr[1]->phi_thuong_nien) : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->phi_thuong_nien) ? number_format($card_arr[2]->phi_thuong_nien) : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->phi_thuong_nien) ? number_format($card_arr[3]->phi_thuong_nien) : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Phí chậm trả nợ tín dụng</th>
                                            <td class="text_Cap item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->phi_cham_tra_no_tin_dung) ? number_format($card_arr[0]->phi_cham_tra_no_tin_dung) : '' }}</td>
                                            <td class="text_Cap item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->phi_cham_tra_no_tin_dung) ? number_format($card_arr[1]->phi_cham_tra_no_tin_dung) : '' }}</td>
                                            <td class="text_Cap item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->phi_cham_tra_no_tin_dung) ? number_format($card_arr[2]->phi_cham_tra_no_tin_dung) : '' }}</td>
                                            <td class="text_Cap item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->phi_cham_tra_no_tin_dung) ? number_format($card_arr[3]->phi_cham_tra_no_tin_dung) : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>phí giao dịch ngoại tệ</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->phi_giao_dich_ngoai_te) ? number_format($card_arr[0]->phi_giao_dich_ngoai_te) : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->phi_giao_dich_ngoai_te) ? number_format($card_arr[1]->phi_giao_dich_ngoai_te) : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->phi_giao_dich_ngoai_te) ? number_format($card_arr[2]->phi_giao_dich_ngoai_te) : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->phi_giao_dich_ngoai_te) ? number_format($card_arr[3]->phi_giao_dich_ngoai_te) : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Phí duy trì thẻ phụ</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->phi_duy_tri_the_phu) ? number_format($card_arr[0]->phi_duy_tri_the_phu) : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->phi_duy_tri_the_phu) ? number_format($card_arr[1]->phi_duy_tri_the_phu) : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->phi_duy_tri_the_phu) ? number_format($card_arr[2]->phi_duy_tri_the_phu) : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->phi_duy_tri_the_phu) ? number_format($card_arr[3]->phi_duy_tri_the_phu) : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Số tiền tối thiểu trả hàng tháng</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->so_tien_toi_thieu_tra_hang_thang) ? number_format($card_arr[0]->so_tien_toi_thieu_tra_hang_thang) : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->so_tien_toi_thieu_tra_hang_thang) ? number_format($card_arr[1]->so_tien_toi_thieu_tra_hang_thang) : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->so_tien_toi_thieu_tra_hang_thang) ? number_format($card_arr[2]->so_tien_toi_thieu_tra_hang_thang) : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->so_tien_toi_thieu_tra_hang_thang) ? number_format($card_arr[3]->so_tien_toi_thieu_tra_hang_thang) : '' }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                            <li>
                                <div class="blockLi_title cf">
                                    <h4>Tiện ích chung</h4>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="table_area" style="overflow-x: auto">
                                    <table class="table_card">
                                        <tr class="text-capitalize">
                                            <th>Trả góp lãi suất 0%</th>
                                            <td class="item-comparison-1"> <?php if(isset($card_arr[0]) && isset($card_arr[0]->tra_gop_lai_suat_0) && $card_arr[0]->tra_gop_lai_suat_0 == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?>  </td>
                                            <td class="item-comparison-2"><?php if(isset($card_arr[1]) && isset($card_arr[1]->tra_gop_lai_suat_0) && $card_arr[1]->tra_gop_lai_suat_0 == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-3"><?php if(isset($card_arr[2]) && isset($card_arr[2]->tra_gop_lai_suat_0) && $card_arr[2]->tra_gop_lai_suat_0 == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-4"><?php if(isset($card_arr[3]) && isset($card_arr[3]->tra_gop_lai_suat_0) && $card_arr[3]->tra_gop_lai_suat_0 == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                        </tr>
                                        <!--tr class="tr_partnerImg">
                                            <th>Ưu đãi đối tác</th>
                                            <td class="item-comparison-1">
                                                <div class="img_partner">
                                                    <div class="img_partner"><img src="images/partner_img01.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img02.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img03.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img04.jpg" alt=""></div>
                                                </div>
                                                <div class="btn_detail"><a href="#">Xem chi tiết</a></div>
                                            </td>
                                            <td class="item-comparison-2">
                                                <div class="img_partner">
                                                    <div class="img_partner"><img src="images/partner_img05.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img06.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img07.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img08.jpg" alt=""></div>
                                                </div>
                                                <div class="btn_detail"><a href="#">Xem chi tiết</a></div>
                                            </td>
                                            <td class="item-comparison-3">
                                                <div class="img_partner">
                                                    <div class="img_partner"><img src="images/partner_img01.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img02.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img03.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img04.jpg" alt=""></div>
                                                </div>
                                                <div class="btn_detail"><a href="#">Xem chi tiết</a></div>
                                            </td>
                                            <td class="item-comparison-4">
                                                <div class="img_partner">
                                                    <div class="img_partner"><img src="images/partner_img01.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img02.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img03.jpg" alt=""></div>
                                                    <div class="img_partner"><img src="images/partner_img04.jpg" alt=""></div>
                                                </div>
                                                <div class="btn_detail"><a href="#">Xem chi tiết</a></div>
                                            </td>
                                        </tr-->
                                        <tr>
                                            <th>Wireless payment</th>
                                            <td class="item-comparison-1"> <?php if(isset($card_arr[0]) && isset($card_arr[0]->wireless_payment) && $card_arr[0]->wireless_payment == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?>  </td>
                                            <td class="item-comparison-2"><?php if(isset($card_arr[1]) && isset($card_arr[1]->wireless_payment) && $card_arr[1]->wireless_payment == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-3"><?php if(isset($card_arr[2]) && isset($card_arr[2]->wireless_payment) && $card_arr[2]->wireless_payment == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-4"><?php if(isset($card_arr[3]) && isset($card_arr[3]->wireless_payment) && $card_arr[3]->wireless_payment == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Bảo hiểm du lịch</th>
                                            <td class="item-comparison-1"> <?php if(isset($card_arr[0]) && isset($card_arr[0]->bao_hiem_du_lich) && $card_arr[0]->bao_hiem_du_lich == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?>  </td>
                                            <td class="item-comparison-2"><?php if(isset($card_arr[1]) && isset($card_arr[1]->bao_hiem_du_lich) && $card_arr[1]->bao_hiem_du_lich == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-3"><?php if(isset($card_arr[2]) && isset($card_arr[2]->bao_hiem_du_lich) && $card_arr[2]->bao_hiem_du_lich == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-4"><?php if(isset($card_arr[3]) && isset($card_arr[3]->bao_hiem_du_lich) && $card_arr[3]->bao_hiem_du_lich == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Dịch vụ hỗ trợ du lịch 24/7</th>
                                            <td class="item-comparison-1"> <?php if(isset($card_arr[0]) && isset($card_arr[0]->ho_tro_du_lich_247) && $card_arr[0]->ho_tro_du_lich_247 == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?>  </td>
                                            <td class="item-comparison-2"><?php if(isset($card_arr[1]) && isset($card_arr[1]->ho_tro_du_lich_247) && $card_arr[1]->ho_tro_du_lich_247 == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-3"><?php if(isset($card_arr[2]) && isset($card_arr[2]->ho_tro_du_lich_247) && $card_arr[2]->ho_tro_du_lich_247 == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-4"><?php if(isset($card_arr[3]) && isset($card_arr[3]->ho_tro_du_lich_247) && $card_arr[3]->ho_tro_du_lich_247 == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Miễn phí phòng chờ sân bay</th>
                                            <td class="item-comparison-1"> <?php if(isset($card_arr[0]) && isset($card_arr[0]->mien_phi_phong_cho_san_bay) && $card_arr[0]->mien_phi_phong_cho_san_bay == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?>  </td>
                                            <td class="item-comparison-2"><?php if(isset($card_arr[1]) && isset($card_arr[1]->mien_phi_phong_cho_san_bay) && $card_arr[1]->mien_phi_phong_cho_san_bay == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-3"><?php if(isset($card_arr[2]) && isset($card_arr[2]->mien_phi_phong_cho_san_bay) && $card_arr[2]->mien_phi_phong_cho_san_bay == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-4"><?php if(isset($card_arr[3]) && isset($card_arr[3]->mien_phi_phong_cho_san_bay) && $card_arr[3]->mien_phi_phong_cho_san_bay == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Hoàn tiền khi trả phí bảo hiểm</th>
                                            <td class="item-comparison-1"> <?php if(isset($card_arr[0]) && isset($card_arr[0]->hoan_tien_khi_tra_phi_bao_hiem) && $card_arr[0]->hoan_tien_khi_tra_phi_bao_hiem == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?>  </td>
                                            <td class="item-comparison-2"><?php if(isset($card_arr[1]) && isset($card_arr[1]->hoan_tien_khi_tra_phi_bao_hiem) && $card_arr[1]->hoan_tien_khi_tra_phi_bao_hiem == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-3"><?php if(isset($card_arr[2]) && isset($card_arr[2]->hoan_tien_khi_tra_phi_bao_hiem) && $card_arr[2]->hoan_tien_khi_tra_phi_bao_hiem == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-4"><?php if(isset($card_arr[3]) && isset($card_arr[3]->hoan_tien_khi_tra_phi_bao_hiem) && $card_arr[3]->hoan_tien_khi_tra_phi_bao_hiem == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Chương trình khách hàng thân thiết</th>
                                            <td class="item-comparison-1"> <?php if(isset($card_arr[0]) && isset($card_arr[0]->khach_hang_than_thiet) && $card_arr[0]->khach_hang_than_thiet == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?>  </td>
                                            <td class="item-comparison-2"><?php if(isset($card_arr[1]) && isset($card_arr[1]->khach_hang_than_thiet) && $card_arr[1]->khach_hang_than_thiet == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-3"><?php if(isset($card_arr[2]) && isset($card_arr[2]->khach_hang_than_thiet) && $card_arr[2]->khach_hang_than_thiet == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-4"><?php if(isset($card_arr[3]) && isset($card_arr[3]->khach_hang_than_thiet) && $card_arr[3]->khach_hang_than_thiet == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Ưu đãi đi Uber / Grab</th>
                                            <td class="item-comparison-1"> <?php if(isset($card_arr[0]) && isset($card_arr[0]->uu_dai_di_uber_grab) && $card_arr[0]->uu_dai_di_uber_grab == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?>  </td>
                                            <td class="item-comparison-2"><?php if(isset($card_arr[1]) && isset($card_arr[1]->uu_dai_di_uber_grab) && $card_arr[1]->uu_dai_di_uber_grab == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-3"><?php if(isset($card_arr[2]) && isset($card_arr[2]->uu_dai_di_uber_grab) && $card_arr[2]->uu_dai_di_uber_grab == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                            <td class="item-comparison-4"><?php if(isset($card_arr[3]) && isset($card_arr[3]->uu_dai_di_uber_grab) && $card_arr[3]->uu_dai_di_uber_grab == ACTIVE) { ?>  <img src="{{ asset('assets/images/tick_ok.png') }}" alt=""> <?php } ?></td>
                                        </tr>
                                        <tr>
                                            <th>Chi tiêu trên một dặm bay</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->chi_tieu_tren_mot_dam_bay) ? $card_arr[0]->chi_tieu_tren_mot_dam_bay : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->chi_tieu_tren_mot_dam_bay) ? $card_arr[1]->chi_tieu_tren_mot_dam_bay : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->chi_tieu_tren_mot_dam_bay) ? $card_arr[2]->chi_tieu_tren_mot_dam_bay : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->chi_tieu_tren_mot_dam_bay) ? $card_arr[3]->chi_tieu_tren_mot_dam_bay : '' }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                            <li>
                                <div class="blockLi_title cf">
                                    <h4>Khuyến mãi mở thẻ</h4>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="table_area" style="overflow-x: auto">
                                    <table class="table_card table_padding">
                                        <tr>
                                            <th>Khuyến mãi mở thẻ</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->khuyen_mai_mo_the_text) ? $card_arr[0]->khuyen_mai_mo_the_text : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->khuyen_mai_mo_the_text) ? $card_arr[1]->khuyen_mai_mo_the_text : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->khuyen_mai_mo_the_text) ? $card_arr[2]->khuyen_mai_mo_the_text : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->khuyen_mai_mo_the_text) ? $card_arr[3]->khuyen_mai_mo_the_text : '' }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                            <li>
                                <div class="blockLi_title cf">
                                    <h4>hồ sơ mở thẻ</h4>
                                    <label class="switch">
                                        <input type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="table_area" style="overflow-x: auto">
                                    <table class="table_card">
                                        <tr>
                                            <th>Chứng minh nhân thân</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->chung_minh_nhan_than) ? $card_arr[0]->chung_minh_nhan_than : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->chung_minh_nhan_than) ? $card_arr[1]->chung_minh_nhan_than : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->chung_minh_nhan_than) ? $card_arr[2]->chung_minh_nhan_than : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->chung_minh_nhan_than) ? $card_arr[3]->chung_minh_nhan_than : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Chứng minh cư trú</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->chung_minh_cu_tru) ? $card_arr[0]->chung_minh_cu_tru : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->chung_minh_cu_tru) ? $card_arr[1]->chung_minh_cu_tru : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->chung_minh_cu_tru) ? $card_arr[2]->chung_minh_cu_tru : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->chung_minh_cu_tru) ? $card_arr[3]->chung_minh_cu_tru : '' }}</td>
                                        </tr>
                                        <tr class="text-capitalize">
                                            <th>Chứng minh thu nhập</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->chung_minh_thu_nhap) ? $card_arr[0]->chung_minh_thu_nhap : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->chung_minh_thu_nhap) ? $card_arr[1]->chung_minh_thu_nhap : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->chung_minh_thu_nhap) ? $card_arr[2]->chung_minh_thu_nhap : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->chung_minh_thu_nhap) ? $card_arr[3]->chung_minh_thu_nhap : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Thẻ đổi thẻ</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->the_doi_the) ? $card_arr[0]->the_doi_the : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->the_doi_the) ? $card_arr[1]->the_doi_the : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->the_doi_the) ? $card_arr[2]->the_doi_the : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->the_doi_the) ? $card_arr[3]->the_doi_the : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Hỗ trợ thu hồ sơ tại nhà</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->thu_ho_so_tai_nha) ? $card_arr[0]->thu_ho_so_tai_nha : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->thu_ho_so_tai_nha) ? $card_arr[1]->thu_ho_so_tai_nha : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->thu_ho_so_tai_nha) ? $card_arr[2]->thu_ho_so_tai_nha : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->thu_ho_so_tai_nha) ? $card_arr[3]->thu_ho_so_tai_nha : '' }}</td>
                                        </tr>
                                        <tr>
                                            <th>Yêu cầu khác</th>
                                            <td class="item-comparison-1">{{ isset($card_arr[0]) && isset($card_arr[0]->yeu_cau_khac) ? $card_arr[0]->yeu_cau_khac : '' }}</td>
                                            <td class="item-comparison-2">{{ isset($card_arr[1]) && isset($card_arr[1]->yeu_cau_khac) ? $card_arr[1]->yeu_cau_khac : '' }}</td>
                                            <td class="item-comparison-3">{{ isset($card_arr[2]) && isset($card_arr[2]->yeu_cau_khac) ? $card_arr[2]->yeu_cau_khac : '' }}</td>
                                            <td class="item-comparison-4">{{ isset($card_arr[3]) && isset($card_arr[3]->yeu_cau_khac) ? $card_arr[3]->yeu_cau_khac : '' }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection