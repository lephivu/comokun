<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/20/16
 * Time: 11:58 PM
 */
?>
@extends('layouts.main_category')

@section('style')

@endsection

@section('content')

    <div class="kv extended mb29">
        <div class="wrapOuter cf">
            <div class="imgCategory00"><img src="{{ asset(PATH_ROOT .'assets/images/img_category_00.png')}}" alt=""></div>
            <div class="kvContent">
                <h2>Thẻ tín dụng để<br><span class="bigger">mua trả góp</span><span class="percent">0%</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>
    </div>
    <div class="wrapOuter category-content">
        <div class="contentArea">
            <div class="wrap cf">
                <div class="banner01">
                    <a href="#" class=""><img src="{{ asset(PATH_ROOT .'assets/images/custom/banner_01.png')}}" alt=""></a>
                    <a href="#" class=""><img src="{{ asset(PATH_ROOT .'assets/images/custom/banner_02.png')}}" alt=""></a>
                </div>
                <div class="sideArea fl">
                    <div class="ttlFilter text-uppercase">Bộ lọc</div>
                    {!! Form::open(array('id' => "filterCardForm", 'method' => 'POST', 'route' => "category", 'class' => '')) !!}
                        <div class="formGroup">
                            <div class="formLabel text-semibold text-uppercase">Phí thường niên (VND)</div>
                            <select name="search_annual_fee" class="select">
                                <option value=""></option>
                                <option value="less250" <?php if(isset($filters['search_annual_fee']) && $filters['search_annual_fee'] == 'less250'){ ?> selected <?php } ?> >Dưới 250,000</option>
                                <option value="250to500" <?php if(isset($filters['search_annual_fee']) && $filters['search_annual_fee'] == '250to500'){ ?> selected <?php } ?> >250,000 đến 500,000</option>
                                <option value="500to1000" <?php if(isset($filters['search_annual_fee']) && $filters['search_annual_fee'] == '500to1000'){ ?> selected <?php } ?> >500,000 đến 1,000,000</option>
                                <option value="larger1000" <?php if(isset($filters['search_annual_fee']) && $filters['search_annual_fee'] == 'larger1000'){ ?> selected <?php } ?> >Trên 1,000,000</option>
                            </select>
                        </div>
                        <div class="formGroup">
                            <div class="formLabel text-semibold text-uppercase">Loại thẻ</div>
                            <div class="checkboxArea is-active">
                                <?php echo make_card_type_checkbox('search_card_type[]', (isset($filters['search_card_type'])) ? $filters['search_card_type'] : [] ) ?>
<!---->
                                {{--<label class="label"><input type="checkbox" name="search_card_type[]" class="checkbox text-uppercase"><span>VISA</span></label>--}}
                                {{--<label class="label"><input type="checkbox" name="search_card_type[]" class="checkbox text-uppercase"><span>Amex</span></label>--}}
                                {{--<label class="label"><input type="checkbox" name="search_card_type[]" class="checkbox text-uppercase"><span>Master Card</span></label>--}}
                                {{--<label class="label"><input type="checkbox" name="search_card_type[]" class="checkbox text-uppercase"><span>Union Pay</span></label>--}}
                                {{--<label class="label"><input type="checkbox" name="search_card_type[]" class="checkbox text-uppercase"><span>JCB</span></label>--}}
                            </div>
                        </div>
                        <div class="formGroup">
                            <div class="formLabel text-semibold text-uppercase">Ngân hàng</div>
                            <select class="select selectRegion">
                                <option value="inter">Quốc tế</option>
                                <option value="vn">Việt Nam</option>
                            </select>
                            <div class="checkboxArea optioninter is-active">
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>HSBC</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Shinhan</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Citibank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Standard Chartered</span></label>
                            </div>
                            <div class="checkboxArea optionvn">
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Vietcombank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Vietinbank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>BIDV</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Agribank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>VPbank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Sacombank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>ACB</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Eximbank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>VIB</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>SHB</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>SCB</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Maritime Bank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>Techcombank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>MBBank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>LienVietPostBank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>HDBank</span></label>
                                <label class="label"><input type="checkbox" name="search_bank[]" class="checkbox"><span>OceanBank</span></label>
                            </div>
                        </div>
                        <div class="formGroup">
                            <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                        </div>

                    {!! Form::close() !!}
                </div>
                <div class="maincontentArea fr">
                    <div class="boxSort cf">
                        <div class="selectSortArea cf">
                            {!! Form::open(array('id' => "sortCardFrm", 'method' => 'POST', 'route' => "category")) !!}

                            {{--<div class="ttlSort text-semibold text-uppercase">Sắp xếp theo</div>--}}
                            <select name="sortCard" class="selectSort" onchange="$('#sortCardFrm').submit();">
                                <option value="">- Sắp xếp theo -</option>
                                <option value="annual_fee_asc" <?php if(isset($filters['sortCard']) && $filters['sortCard'] == 'annual_fee_asc'){ ?> selected <?php } ?> >Phí thường niên (Nhỏ trước)</option>
                                <option value="annual_fee_desc" <?php if(isset($filters['sortCard']) && $filters['sortCard'] == 'annual_fee_desc'){ ?> selected <?php } ?> >Phí thường niên (Lớn trước)</option>
                                <option value="popularity" <?php if(isset($filters['sortCard']) && $filters['sortCard'] == 'popularity'){ ?> selected <?php } ?> >Popularity (phổ biến)</option>
                                <option value="interest_rate_asc" <?php if(isset($filters['sortCard']) && $filters['sortCard'] == 'interest_rate_asc'){ ?> selected <?php } ?> >Lãi suất (Nhỏ trước)</option>
                                <option value="interest_rate_desc" <?php if(isset($filters['sortCard']) && $filters['sortCard'] == 'interest_rate_desc'){ ?> selected <?php } ?> >Lãi suất (Lớn trước)</option>
                            </select>

                            {!! Form::close() !!}
                        </div>
                        <div class="gridshowArea cf">
                            <div class="gridByRow fl"></div>
                            <div class="gridByColumn fl is-active"></div>
                        </div>
                    </div>
                    <ul class="cardList cf pageCategory">
                        @foreach($cards_arr as $card)

                            <li class="card">
                                <div class="cardBody">
                                    <div class="cf">
                                        <div class="img_card fl">
                                            <img src="{{ PATH_IMAGE.$card->image }}" alt="" class="w100p">
                                            <div class="iconSpecial cf">
                                                <img src="{{ asset(PATH_ROOT .'assets/images/icon_panther_special.png')}}" alt="" class="icon_panther_special">
                                                <p>Lựa chọn của báo đen</p>
                                            </div>
                                        </div>
                                        <div class="cardContent">
                                            <h4 class="ttl text-semibold">{{ $card->ten_the or '' }}</h4>
                                            <ul class="starList cf">
                                                <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_star.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_star_off.png')}}" alt=""></li>
                                                <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_star.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_star_off.png')}}" alt=""></li>
                                                <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_star.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_star_off.png')}}" alt=""></li>
                                                <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_star.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_star_off.png')}}" alt=""></li>
                                                <li class="star"><img src="{{ asset(PATH_ROOT .'assets/images/icon_star.png')}}" alt=""><img src="{{ asset(PATH_ROOT .'assets/images/icon_star_off.png')}}" alt=""></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="cardNote">
                                        <p class="textPromo text-semibold">Promo</p>
                                        <p class="text text-semibold color-1"><?php echo strlen($card->khuyen_mai_mo_the_text) <= 170 ? $card->khuyen_mai_mo_the_text : substr($card->khuyen_mai_mo_the_text, 0, 170)."..." ?></p>
                                    </div>
                                    <ul class="infoList">
                                        <li class="info v01 color-1">Hạn mức tín dụng: <?php echo isset($card->han_muc_tin_dung) ? number_format($card->han_muc_tin_dung): 0?> VND</li>
                                        <li class="info v02 color-1">Yêu cầu thu nhập: <?php echo isset($card->yeu_cau_thu_nhap) ? number_format($card->yeu_cau_thu_nhap) : 0 ?> VND</li>
                                        <li class="info v03 color-1">Phí thường niên: <?php echo isset($card->phi_thuong_nien) ? number_format($card->phi_thuong_nien) : 0 ?> VND<br>(Miễn phí năm đầu)</li>
                                    </ul>
                                    <div class="actionBtnList cf">
                                        <div class="fl likeBtn text-semibold">Yêu thích</div>
                                        <div class="fl compareBtn text-semibold text-uppercase" data-href-add="{{ url('comparison_add/'.$card->id) }}">So sánh</div>
                                    </div>
                                </div>
                                <div class="cardFooter">
                                    <ul class="btnList cf">
                                        <li class="fl btn v01 text-center text-semibold"><a href="#" class="btn-home-detail" href="javascript:void(0);" data-href="{{ asset("card/".$card->id) }}">Xem chi tiết</a></li>
                                        <li class="fr btn v02 text-center text-semibold"><a href="javascript:void(0)" data-toggle="modal" data-target="#login-modal" class="register-button" >Đăng ký ngay</a></li>
                                    </ul>
                                </div>
                            </li>
                        @endforeach

                    </ul>
                    <div class="pagination-container">
                        {{ $result->links("pagination") }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--compare-->
    <div class="block-modal-comparison">
        <div class="container-fluid">
            <div class="col-md-12 block-content-modal-compare">
                <ul class="list-product-compare col-md-10">
                    @include('partials/comparison_modal')
                </ul>
                <div class="col-md-2">
                    <div class="btn-wrap-compare">
                        <a href="{{ url('comparison') }}" class="category-btn-compare">So sánh</a>
                        <a href="javascript:void(0);" class="category-btn-clear">Chọn lại</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modalDetail" id="popupDetail">
        <div class="modal-dialog wrapDetail">
            <div class="kvDetail">
                <img src="{{ asset('assets/images/bg_category_detail.png') }}" alt="" class="w100p">
                <a href="#" class="btnCloseDetail"><img src="{{ asset('assets/images/icon_close_detail.png') }}" alt=""></a>
            </div>

            <div class="contentDetail cf">
                <div id="cardInfo">
                    @include('card_detail')
                </div>

                <div class="relativeCard">
                    <div class="box04">
                        <h2 class="textRelated text-semibold text-uppercase text-center"><span>Sản phẩm tương tự</span></h2>
                        <ul class="relatedSlider cf">
                            @foreach($card_relative_arr as $card)
                                <li class="cardRelated text-center">
                                    <h4 class="cardTitle"><a class="btn-home-detail" href="javascript:void(0);" data-href="{{ asset("card/".$card->id) }}"> {{$card->ten_the}} </a></h4>
                                    <a class="btn-home-detail" href="javascript:void(0);" data-href="{{ asset("card/".$card->id) }}"><img src="{{ PATH_IMAGE.$card->image }}" alt=""></a>
                                </li>
                            @endforeach

                        </ul>
                        <div class="relatedSliderDots"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{--<div class="chat_with_baoden">--}}
        {{--<div class="ttl_chat">--}}
            {{--<div class="miz_chat01"><img src="{{ asset(PATH_ROOT .'assets/images/miz_chat01.png')}}" alt=""></div>--}}
            {{--<p>Trò chuyện cùng Báo Đen</p>--}}
            {{--<div class="miz_chat02"><img src="{{ asset(PATH_ROOT .'assets/images/miz_chat02.png')}}" alt=""></div>--}}
        {{--</div>--}}
        {{--<div class="content_chat">--}}
            {{--<p class="p_time_today">Today at 10:14 AM</p>--}}
            {{--<div class="admin_chat">--}}
                {{--<p class="admin_img_chat"><img src="{{ asset(PATH_ROOT .'assets/images/admin_img_chat.png')}}" alt=""></p>--}}
                {{--<p class="admin_txt_chat">Xin chào! Báo Đen có thể giúp gì cho bạn?</p>--}}
            {{--</div>--}}
            {{--<p class="p_time_sent">Sent 10:14 am</p>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection

@section('script')
    <script>

        /* ==== Start Home Page ==== */
        var slicked = false;
        $('.btn-home-detail').click(function(e) {
            e.preventDefault();

            var dataURL = $(this).attr('data-href');

            // load data from database
            $('#cardInfo').load(dataURL, function(){
                $("[tab].is-active>[tab-body]").slideDown(0);
                $("[tab-header-pc]").on("click", function() {
                    var index = $(this).index(),
                            tablist = $(this).closest("[headerlist]").next(),
                            tabActive = tablist.find("[tab]").eq(index);
                    $(this).siblings().removeClass("is-active");
                    $(this).parent().children().eq(index).addClass("is-active");
                    tablist.find("[tab]").removeClass("is-active");
                    tabActive.addClass("is-active");
                    tabActive.siblings().children("[tab-body]").slideUp(0);
                    tabActive.children("[tab-body]").slideDown(0);

                });
                $("[tab-header-sp]").on("click", function() {
                    var index = $(this).closest("[tab]").index(),
                            tab = $(this).parent();
                    headerActive = $(this).closest("[tab-container]").children("[headerlist]").find("[tab-header-pc]").eq(index);
                    tab.siblings().removeClass("is-active");
                    tab.toggleClass("is-active");
                    tab.siblings().find("[tab-body]").slideUp(500);
                    $(this).next().slideToggle(500);
                    headerActive.siblings().removeClass("is-active");
                    headerActive.toggleClass("is-active");
                });

                $(".tabBody>.inner").mCustomScrollbar({
                    theme: "rounded-dots",
                    mouseWheelPixels: 100,
                    scrollInertia: 0
                });



            });


            // open modal
            $('.modalDetail').addClass('is-active');
            $('body').addClass('modal-open');

            if (!slicked) {
                $('.relatedSlider').slick({
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: true,
                    prevArrow: '<span class="fa fa-angle-left slick-prev"></span>',
                    nextArrow: '<span class="fa fa-angle-right slick-next"></span>',
                    dots: true,
                    appendDots: '.relatedSliderDots',
                    infinite: true,
                    responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                        {
                            breakpoint: 481,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            }
            slicked = true;


        });
        $('.btnCloseDetail').click(function(e) {
            e.preventDefault();
            $('.modalDetail').removeClass('is-active');
            $('body').removeClass('modal-open');
        });
        $(window).click(function() {
            $('.modalDetail').removeClass('is-active');
            $('body').removeClass('modal-open');
        });
        $('.wrapDetail, .btn-home-detail').click(function(e) {
            e.stopPropagation();
        });
        $(window).on('keydown', function(e) {
            var keycode = e.keyCode;
            if (keycode === 27) {
                $('.modalDetail').removeClass('is-active');
                $('body').removeClass('modal-open');
            }
        });

        /* ==== End Home Page ==== */


        //compare modal
        $('.compareBtn').click(function() {
            $(this).toggleClass('is-active');
            if(  $(".block-modal-comparison").is(":visible") != true ) {
                $(".block-modal-comparison").toggleClass('is-active');
            }

            var addURL = $(this).attr('data-href-add');
            // load data from database
            $('.list-product-compare').load(addURL, function(){
                /*
                //event on remove card for comparision
                $('.close_compare').click(function() {
                    //$(this).closest('.list-product-compare li .block-item-product').removeClass("is-active");
                    // load data from database
                    var removeURL = $(this).attr('data-href-remove');
                    alert('removeURL'+removeURL);
                    $('.list-product-compare').load(removeURL, function(){

                    });
                });
                */
            });
        });

        $(".category-btn-clear").click(function(){
            $(".block-modal-comparison").toggleClass('is-active');
        });

    </script>
@endsection