
<div class="wrapDetailInner">
    <div class="box01 cf" style="margin: 20px 0px;">
        <div class="logo_detail">
            <img src="{{ asset('assets/images/logo/HSBC_logo_detail.png') }}">
        </div>
        <div class="cardImgArea fl">
            <div class="cardTitle01"> {{ $card->bankInfo->ten_ngan_hang or '' }}</div>
            <img src="{{ PATH_IMAGE.$card->image }}" alt="" class="cardImg01">
        </div>
        <div class="cardContent01 fl">
            <div class="cardName">{{ $card->ten_the or '' }}</div>
            <ul class="starList cf">
                <li class="star"><img src="{{ asset("assets/images/icon_star.png") }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                <li class="star"><img src="{{ asset("assets/images/icon_star.png") }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                <li class="star"><img src="{{ asset("assets/images/icon_star.png") }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                <li class="star"><img src="{{ asset("assets/images/icon_star.png") }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
                <li class="star"><img src="{{ asset("assets/images/icon_star.png") }}" alt=""><img src="{{ asset('assets/images/icon_star_off.png') }}" alt=""></li>
            </ul>
        </div>
        <div class="btnArea fr">
            <a href="./category.html" class="btnCompare">So sánh</a>
            <a href="./form.html" class="btnRegister">Đăng ký ngay</a>
        </div>
    </div>
    <div class="box02">
        <p class="textPromo text-semibold text-uppercase v02">Promo</p>
        <div class="blockPromoWrap cf">
            <div class="blockPromo cf fl">
                <img src="{{ asset('assets/images/img_promo_01.jpg')}}" alt="" class="imgPromo fl">
                <div class="content">
                    <h4 class="title text-semibold">Bắt trọn <br class="hide-sp">mọi khoảnh khắc</h4>
                    <p class="text">Hãy chi tiêu cùng Thẻ tín dụng HSBC khi bạn ở bất cứ đâu trong dịp Tết này để có cơ hội nhận ngay máy ảnh hành động sắc nét trị giá 1.000.000 VND.</p>
                </div>
            </div>
            <div class="blockPromo cf fl">
                <img src="{{ asset('assets/images/img_promo_02.jpg')}} " alt="" class="imgPromo fl">
                <div class="content">
                    <h4 class="title text-semibold">Ưu đãi tại Coffee Bean <br class="hide-sp">&amp; Tea Leaf</h4>
                    <p class="text">Hãy chi tiêu cùng Thẻ tín dụng HSBC khi bạn ở bất cứ đâu trong dịp Tết này để có cơ hội nhận ngay máy ảnh hành động sắc nét trị giá 1.000.000 VND.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="box03" style="padding-left: 0px; padding-right: 0px; border-bottom: none">

        <div tab-container="vertical" class="table">
            <ul headerlist class="tabHeaderList cell">
                <li tab-header-pc class="is-active"><span>Chi tiết</span></li>
                <li tab-header-pc><span>Tiện ích</span></li>
                <li tab-header-pc><span>Đối tác</span></li>
            </ul>
            <ul tablist class="tabList cell">
                <li tab class="is-active">
                    <div tab-header-sp class="tabHeader-sp"><span>Chi tiết</span></div>
                    <div tab-body class="tabBody">
                        <div class="inner">
                            <div class="infoArea">
                                <h4 class="bodyTitle">Thông tin nổi bật</h4>
                                <dl class="cf">
                                    <dt>Tổ chức thẻ</dt>
                                    <dd>{{ isset($card->cardTypeInfo) ? $card->cardTypeInfo->ten_loai : '' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Lãi suất( tháng)</dt>
                                    <dd><?php echo isset($card->lai_suat) ? number_format($card->lai_suat) : '' ?></dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Số ngày ân hạn</dt>
                                    <dd>{{ isset($card->so_ngay_an_han) ? $card->so_ngay_an_han : '' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Hạn mức tín dụng</dt>
                                    <dd><?php echo !empty($card->han_muc_tin_dung) && $card->han_muc_tin_dung > 0 ? number_format($card->han_muc_tin_dung) : (empty($card->han_muc_tin_dung) ? '-' : 'VND') ?> </dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Hoàn tiền cao nhất</dt>
                                    <dd>{{ isset($card->hoan_tien_cao_nhat) ? $card->hoan_tien_cao_nhat : '' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Yêu cầu thu nhập</dt>
                                    <dd><?php echo isset($card->yeu_cau_thu_nhap) ? number_format($card->yeu_cau_thu_nhap) : '' ?> VND</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Hạn mức rút tiền mặt </dt>
                                    <dd><?php echo isset($card->han_muc_rut_tien_mat) ? number_format($card->han_muc_rut_tien_mat) : '' ?> VND</dd>
                                </dl>
                            </div>
                            <div class="infoArea">
                                <h4 class="bodyTitle">Các loại phí</h4>
                                <dl class="cf">
                                    <dt>Phí phát hành</dt>
	                                    <dd><?php echo !empty($card->phi_phat_hanh) && $card->phi_phat_hanh > 0 ? number_format($card->phi_phat_hanh) : (empty($card->phi_phat_hanh) ? '-' : 'VND') ?> </dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Phí thường niên</dt>
                                    <dd><?php echo isset($card->phi_thuong_nien) ? number_format($card->phi_thuong_nien) : '' ?> VND</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Phí ứng tiền mặt</dt>
                                    <dd><?php echo isset($card->phi_ung_tien_mat) ? number_format($card->phi_ung_tien_mat) : '' ?> VND</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Phí chậm trả nợ tín dụng</dt>
                                    <dd><?php echo isset($card->phi_cham_tra_no_tin_dung) ? number_format($card->phi_cham_tra_no_tin_dung) : '' ?> VND</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Phí giao dịch ngoại tệ</dt>
                                    <dd><?php echo isset($card->phi_giao_dich_ngoai_te) ? number_format($card->phi_giao_dich_ngoai_te) : '' ?> VND</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Phí duy trì thẻ phụ</dt>
                                    <dd><?php echo isset($card->phi_duy_tri_the_phu) ? number_format($card->phi_duy_tri_the_phu) : '' ?> VND</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Mức phí tối thiểu trả hàng tháng</dt>
                                    <dd><?php echo isset($card->so_tien_toi_thieu_tra_hang_thang) ? number_format($card->so_tien_toi_thieu_tra_hang_thang) : '' ?> VND <span class="text-italic">(Miễn phí năm đầu)</span></dd>
                                </dl>
                            </div>
                            <div class="infoArea">
                                <h4 class="bodyTitle">Hồ sơ mở thẻ</h4>
                                <dl class="cf">
                                    <dt>Chứng minh nhân thân</dt>
                                    <dd>{{ isset($card->chung_minh_nhan_than) ? $card->chung_minh_nhan_than : '' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Chứng minh cư trú</dt>
                                    <dd>{{ isset($card->chung_minh_cu_tru) ? $card->chung_minh_cu_tru : '' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Chứng minh thu nhập</dt>
                                    <dd>{{ isset($card->chung_minh_thu_nhap) ? $card->chung_minh_thu_nhap : '' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Thẻ đổi thẻ</dt>
                                    <dd>{{ isset($card->the_doi_the) ? $card->the_doi_the : '' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Hỗ trợ thu hồ sơ tại nhà</dt>
                                    <dd>{{ isset($card->thu_ho_so_tai_nha) ? $card->thu_ho_so_tai_nha : '' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Yêu cầu khác</dt>
                                    <dd>{{ isset($card->yeu_cau_khac) ? $card->yeu_cau_khac : '' }}</dd>
                                </dl>
                            </div>
                            <div class="infoArea">
                                <h4 class="bodyTitle">Khác</h4>
                                <dl class="cf">
                                    <dt>Khuyến mãi</dt>
                                    <dd>{!! isset($card->khuyen_mai) ? $card->khuyen_mai : '' !!}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </li>
                <li tab>
                    <div tab-header-sp class="tabHeader-sp"><span>Tiện ích</span></div>
                    <div tab-body class="tabBody">
                        <div class="inner">
                            <div class="infoArea">
                                <dl class="cf">
                                    <dt>Trả góp lãi suất 0%</dt>
                                    <dd>{{ isset($card->tra_gop_lai_suat_0) && $card->tra_gop_lai_suat_0 == ACTIVE ? 'Có' : 'Không' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Wireless payment</dt>
                                    <dd>{{ isset($card->wireless_payment) && $card->wireless_payment == ACTIVE ? 'Có' : 'Không' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Bảo hiểm du lịch</dt>
                                    <dd>{{ isset($card->bao_hiem_du_lich) && $card->bao_hiem_du_lich == ACTIVE ? 'Có' : 'Không' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Dịch vụ hỗ trợ du lịch 24/7</dt>
                                    <dd>{{ isset($card->ho_tro_du_lich_247) && $card->ho_tro_du_lich_247 == ACTIVE ? 'Có' : 'Không' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Miễn phí phòng chờ sân bay</dt>
                                    <dd>{{ isset($card->mien_phi_phong_cho_san_bay) && $card->mien_phi_phong_cho_san_bay == ACTIVE ? 'Có' : 'Không' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Hoàn tiền khi trả phí bảo hiểm</dt>
                                    <dd>{{ isset($card->hoan_tien_khi_tra_phi_bao_hiem) && $card->hoan_tien_khi_tra_phi_bao_hiem == ACTIVE ? 'Có' : 'Không' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Chương trình khách hàng thân thiết</dt>
                                    <dd>{{ isset($card->khach_hang_than_thiet) && $card->khach_hang_than_thiet == ACTIVE ? 'Có' : 'Không' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Ưu đãi đi Uber / Grab</dt>
                                    <dd>{{ isset($card->uu_dai_di_uber_grab) && $card->uu_dai_di_uber_grab == ACTIVE ? 'Có' : 'Không' }}</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Chi tiêu trên một dặm bay</dt>
                                    <dd>{{ isset($card->chi_tieu_tren_mot_dam_bay) ? $card->chi_tieu_tren_mot_dam_bay : '' }}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </li>
                <li tab>
                    <div tab-header-sp class="tabHeader-sp"><span>Đối tác</span></div>
                    <div tab-body class="tabBody">
                        <div class="inner">
                            <div class="infoArea">
                                Danh sách đối tác
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

    </div>

</div>
