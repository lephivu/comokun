<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/31/16
 * Time: 2:23 AM
 */
?>

@extends('layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/contact.css') }}">

@endsection

@section('content')
    <div class="wrapOuterInfo">
        <div class="contactArea">
            <div class="wrapContact cf">
                <div class="contactInner">
                    <div class="left_contact">
                        <div class="left_contact_inner">
                            <p class="contact_img"><img src="{{ asset('assets/images/contact/contact_img.jpg') }}" alt=""></p>
                            <h4 class="contact_info_ttl">Thông tin liên lạc:</h4>
                            <p class="contact_add">Tầng 10, Toà nhà SCETPA, 19A,<br class="hide_sp">Cộng Hoà, P.12, Q.Tân Bình, TP. HCM</p>
                            <p class="contact_email">info@comokun.vn</p>
                            <p class="contact_phone">( + 84 ) 28 7302 4248</p>
                        </div>
                    </div>
                    <div class="infoContactArea">
                        <div class="infoContact">
                            <form action="#" method="get">
                                <div class="infoFormInner">
                                    <p class="info_title">Họ tên của bạn <span class="color_red">(*)</span></p>
                                    <input type="text" name="">
                                    <p class="info_title">Số điện thoại <span class="color_red">(*)</span></p>
                                    <input type="text" name="">
                                    <p class="info_title">Email của bạn</p>
                                    <input type="text" name="">
                                    <p class="info_title">Ghi chú</p>
                                    <textarea name="note_contact" class="note_contact" cols="30" rows="10"></textarea>
                                    <button type="submit" class="btnContactInfo">Gửi</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

@endsection