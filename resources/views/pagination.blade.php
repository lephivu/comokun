@if ($paginator->hasPages())
    <ul class="list-inline list-unstyled">
        @if ($paginator->onFirstPage())
            {{--<li class="disabled"><span><img src="{{ asset('assets/images/promotion/prev.png') }}"></span></li>--}}
        @else
            <li class="prev"><a href="{{ $paginator->previousPageUrl() }}"><span><img src="{{ asset('assets/images/promotion/prev.png') }}"></span></a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><span>{{ $page }}</span></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="next"><a href="{{ $paginator->nextPageUrl() }}" ><span><img src="{{ asset('assets/images/promotion/next.png') }}"></span></a></li>
        @else
                {{--<li class="disabled"><span><img src="{{ asset('assets/images/promotion/next.png') }}"></span></li>--}}
        @endif
    </ul><!-- /.list-inline -->
@endif