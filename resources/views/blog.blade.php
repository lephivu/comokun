<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/31/16
 * Time: 2:23 AM
 */
?>

@extends('layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/blog.css') }}">
@endsection

@section('content')
    <div class="wrapOuter">
        <div class="wrap">
            <div class="kv"><img src="{{ asset('assets/images/kv_blog.jpg') }}" alt="" class="w100p"></div>
            <div class="blogArea">
                <ul class="blogList cf">
                    <li class="blog">
                        <div class="blogImg"><a href="#"><img src="{{ asset('assets/images/img_blog_01.jpg') }}" alt=""></a></div>
                        <div class="blogContent">
                            <h4 class="blogName"><a href="#">Có bảo hiểm nhân thọ, cha mẹ đâu đến mức vay nợ để con được học đại học</a></h4>
                            <div class="date">28/09/2016</div>
                        </div>
                    </li>
                    <li class="blog">
                        <div class="blogImg"><a href="#"><img src="{{ asset('assets/images/img_blog_02.jpg') }}" alt=""></a></div>
                        <div class="blogContent">
                            <h4 class="blogName"><a href="#">Tôi đã không còn điều gì nuối tiếc khi về già bởi tôi có bảo hiểm nhân thọ</a></h4>
                            <div class="date">28/09/2016</div>
                        </div>
                    </li>
                    <li class="blog">
                        <div class="blogImg"><a href="#"><img src="{{ asset('assets/images/img_blog_03.jpg') }}" alt=""></a></div>
                        <div class="blogContent">
                            <h4 class="blogName"><a href="#">4 điều cần lưu ý khi vay tiền bạn bè, người thân để mua nhà</a></h4>
                            <div class="date">28/09/2016</div>
                        </div>
                    </li>
                    <li class="blog">
                        <div class="blogImg"><a href="#"><img src="{{ asset('assets/images/img_blog_04.jpg') }}" alt=""></a></div>
                        <div class="blogContent">
                            <h4 class="blogName"><a href="#">Những điều cần biết về vay theo lương</a></h4>
                            <div class="date">28/09/2016</div>
                        </div>
                    </li>
                    <li class="blog">
                        <div class="blogImg"><a href="#"><img src="{{ asset('assets/images/img_blog_05.jpg') }}" alt=""></a></div>
                        <div class="blogContent">
                            <h4 class="blogName"><a href="#">Vay 500 triệu mua nhà thì cần phải chuẩn bị những gì?</a></h4>
                            <div class="date">28/09/2016</div>
                        </div>
                    </li>
                    <li class="blog">
                        <div class="blogImg"><a href="#"><img src="{{ asset('assets/images/img_blog_06.jpg') }}" alt=""></a></div>
                        <div class="blogContent">
                            <h4 class="blogName"><a href="#">Làm thế nào để chi tiêu tiết kiệm bằng thẻ tín dụng?</a></h4>
                            <div class="date">28/09/2016</div>
                        </div>
                    </li>
                    <li class="blog">
                        <div class="blogImg"><a href="#"><img src="{{ asset('assets/images/img_blog_07.jpg') }}" alt=""></a></div>
                        <div class="blogContent">
                            <h4 class="blogName"><a href="#">Điện thoại từ 3 triệu đồng sẽ được trả góp 0% khi mua bằng thẻ tín dụng</a></h4>
                            <div class="date">28/09/2016</div>
                        </div>
                    </li>
                    <li class="blog">
                        <div class="blogImg"><a href="#"><img src="{{ asset('assets/images/img_blog_08.jpg') }}" alt=""></a></div>
                        <div class="blogContent">
                            <h4 class="blogName"><a href="#">Bạn có sẵn sàng thanh toán bằng thẻ ATM tại siêu thị thay cho tiền mặt không?</a></h4>
                            <div class="date">28/09/2016</div>
                        </div>
                    </li>
                    <li class="blog">
                        <div class="blogImg"><a href="#"><img src="{{ asset('assets/images/img_blog_09.jpg') }}" alt=""></a></div>
                        <div class="blogContent">
                            <h4 class="blogName"><a href="#">Hãy chắc rằng bạn đã đăng ký Verified by VISA trước khi mua hàng trực tuyến</a></h4>
                            <div class="date">28/09/2016</div>
                        </div>
                    </li>
                </ul>
                <div class="text-center">
                    <ul class="pagination text-center">
                        <li class="prev"><a href="#"><img src="{{ asset('assets/images/icon_arrow_left_blog.png') }}" alt=""></a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li class="is-active"><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                        <li><a href="#">8</a></li>
                        <li><a href="#">9</a></li>
                        <li><a href="#">10</a></li>
                        <li class="next"><a href="#"><img src="{{ asset('assets/images/icon_arrow_right_blog.png') }}" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection