<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 5/22/16
 * Time: 3:23 PM
 */
//get the last-modified-date of this very file
$lastModified = filemtime(__FILE__);
//get a unique hash of this file (etag)
$etagFile = md5_file(__FILE__);
//get the HTTP_IF_MODIFIED_SINCE header if set
$ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
//get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
$etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

//set last-modified header
header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModified) . " GMT");
//set etag-header
header("Etag: $etagFile");
//make sure caching is turned on
header('Cache-Control: public, max-age=3600');

?>
        <!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>

    <title> Comokun - Category </title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="Comokun dịch vụ mở thẻ tín dụng" name="description"/>
    <meta content="http://comokun.com/" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('transcosmos.ico')}}">

    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/font-awesome.min.css') }}">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/slick.css') }}">

    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/nouislider.css') }}">


    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/nouislider.css') }}">
    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/common.css') }}">
    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/pagination-custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset(PATH_ROOT.'assets/plugins/owlcarousel/assets/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset(PATH_ROOT.'assets/plugins/owlcarousel/assets/owl.carousel.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset(PATH_ROOT.'assets/plugins/owlcarousel/assets/owl.theme.default.min.css') }}" />
    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/category.css') }}">

    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/page-acount.css') }}">

    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/popup-detail.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset(PATH_ROOT.'assets/css/custom.css') }}">

    @yield('style')
</head>
<body class="category-page">

<!-- begin #sidebar -->
@include('partials.header_category')
<!-- end #sidebar -->

<!-- begin #content -->
@yield('content')

<!-- end #content -->

@include('partials.footer')


<script src="{{ asset(PATH_ROOT.'assets/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/js/slick-1.6.0/slick.min.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/js/nouislider.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/js/jquery.form.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/js/wNumb.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/plugins/pagination/jquery.simplePagination.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/plugins/owlcarousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/js/script.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/js/custom.js') }}"></script>
<script src="{{ asset(PATH_ROOT.'assets/js/scrollbar.js') }}"></script>

@yield('script')


</body>
</html>

