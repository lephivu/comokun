@extends('layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/about-us.css')}} ">

@endsection

@section('content')
    <div class="about_wrap">
        <section class="aboutHeadArea container-fluid">
            <div class="container">
                <div class="row aboutHead">
                    <div class="about_comoImg"><img src="{{ asset('assets/images/about_comoImg.png') }}" alt=""></div>
                    <div class="about_comokunArea">
                        <div class="about_comokun">
                            <h2>comokun là ai?</h2>
                            <p>Là chàng báo đen đến từ Nhật bản, rất thông minh và nhanh nhẹn, tên là Como, hay được bạn bè gọi là Comokun. Trong tiếng Nhật, "kun" là cách gọi yêu cho các cậu bé, hoặc chàng trai trẻ. <br><br> 
	                            Tên này thuộc bản quyền của công ty transcosmos Việt Nam - là nhà tài trợ website này, vừa là viết tắt của <span style="text-decoration: underline">CO</span>mparison <span style="text-decoration: underline">MO</span>bility - nghĩa là sự So Sánh Linh Động các loại thẻ tín dụng. Comokun sẽ giúp bạn tìm hiểu và lựa chọn được thẻ phù hợp với mình nhất!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container px-0">
            <div class="row why_choose">
                <h2>Tại sao gọi là giải pháp Báo Đen?</h2>
                <p class="why_choose_txt">Giải pháp Báo Đen Comokun là website chuyên so sánh các đặc tính quan trọng của thẻ tín dụng. Không chỉ có đầy đủ thông tin về nhiều loại thẻ của nhiều ngân hàng hiện có trên thị trường, website còn có nhiều tín năng đặc biệt giúp bạn
                    tương tác và có được trải nghiệm người dùng tốt nhất, như là Chat với Comokun, được gọi điện thoại tư vấn trực tiếp, ứng dụng trang cá nhân với những ưu đãi khuyến mãi dành riêng cho bạn, tích điểm đổi lấy nhiều voucher hấp dẫn...
                    Website liên tục được cập nhật, nên bạn hãy thường xuyên sử dụng site để khám phá những tín năng mới thông minh của Comokun nhé!</p>
                <ul class="why_choose_img">
                    <li>
                        <div class="img-why"><img src="{{ asset('assets/images/about_us/why_choose_img01.png') }}" alt=""></div>
                        <div class="why-description">
                            Tiết kiệm thời gian tìm hiểu vì Comokun hiển thị đa dạng các loại thẻ.
                        </div>
                    </li>
                    <li>
                        <div class="img-why"><img src="{{ asset('assets/images/about_us/why_choose_img02.png') }}" alt=""></div>
                        <div class="why-description">
                            Thông tin luôn được cập nhật
                        </div>
                    </li>
                    <li>
                        <div class="img-why"><img src="{{ asset('assets/images/about_us/why_choose_img03.png') }}" alt=""></div>
                        <div class="why-description">
                            Công cụ so sánh thông minh giúp bạn chọn lọc thẻ thích hợp nhất với nhu cầu cá nhân
                        </div>
                    </li>
                    <li>
                        <div class="img-why"><img src="{{ asset('assets/images/about_us/why_choose_img04.png') }}" alt=""></div>
                        <div class="why-description">
                            Website được trang bị nhiều tầng an ninh, giúp bảo vệ thông tin cho bạn
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="about-attractive">
            <div class="block-about-attractive">
                <img class="img-attractive" src="{{ asset('assets/images/about_us/img-attractive.png') }}">
                <div class="attractive-description">
                    <p class="title">
                        Nhiều ưu đãi hấp dẫn
                    </p>
                    <p class="text">
                        từ các đối tác thân thiết của Comokun đáng yêu
                    </p>
                </div>
            </div>
            <div class="block-about-consult">
                <div class="consult-description">
                    Nếu bạn phân vân giữa nhiều lựa chọn hấp dẫn, bạn có thể chat hoặc yêu cầu tư vấn trực tiếp miễn phí!
                </div>
                  <a href="#" class="btn-chat">
                    Chat với Comokun
                </a>
                <a href="#" class="btn-request">
                    Yêu cầu tư vấn
                </a>
            </div>
        </div>
        <div class="container">
            <div class="introduct-about-title">
                VỀ CÔNG TY TNHH TRANSCOSMOS VIỆT NAM
            </div>
            <div class="row introduct-description">
                <div class="col-md-4 introduct-img">
                    <img src="{{ asset('assets/images/about_us/about-introduct.png') }}">
                </div>
                <div class="col-md-8 introduct-about-text">
                    <p>
                        Công ty <b>TNHH TRANSCOSMOS VIỆT NAM</b> là công ty 100% vốn Nhật Bản, chuyên cung cấp dịch vụ ở nhiều mảng bao gồm Chăm sóc Khách hàng qua Điện thoại (Call Center), BPO (Business Process Outsourcing), Tiếp thị Số (Digital Marketing)
                        và các Giải pháp Điện tử (Engineering Solution). Công ty được thành lập ở Việt Nam vào tháng 3 năm 2014, nhưng tập đoàn transcosmos đã có chiều dài lịch sử vận hành những dịch vụ này từ năm 1966.
                    </p>
                    <p>
                        <b>TRANSCOSMOS VIỆT NAM</b> cam kết cung cấp dịch vụ chất lượng cao và ngày càng mở rộng nhiều mảng dịch vụ để mang lại sự hài lòng trọn vẹn cho khách hàng.
                    </p>
                </div>
            </div>
        </div>
        <div class="container fb_customer">
            <div class="row">
                <div class="ttl_fb_customer">
                    <h2>cảm nhận của khách hàng</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
                <div class="col-ls-6 col-md-6 col-xs-12 col_fb_customer">
                    <div class="img_fb_cus"> <img src="{{ asset('assets/images/img_cus01.png') }}" alt=""> </div>
                    <div class="txt_fb_customer">
                        <p class="txt_fb_cus">Tôi đánh giá đây là giải pháp đón đầu xu hướng tiêu dùng của thị trường Việt Nam, không chỉ cung cấp những thông tin hữu ích và công cụ so sánh tối ưu cho các loại thẻ tín dụng trên thị trường mà còn đem đến những ưu đãi hấp dẫn
                            cho người dùng nhằm mục đích gia tăng dân số “người tiêu dùng thông minh”. Một giải pháp tuyệt vời cho những ai không muốn bỏ lại sau trong bối cảnh phát triển số hóa ngày nay.</p>
                        <p class="name_fb_cus">Yohei Koumura</p>
                        <p class="pos_fb_cus">Tổng Giám Đốc <span>@ transcosmos Vietnam Co., Ltd</span></p>
                    </div>
                </div>
                <div class="col-ls-6 col-md-6 col-xs-12 col_fb_customer">
                    <div class="img_fb_cus"> <img src="{{ asset('assets/images/img_cus02.png') }}" alt=""> </div>
                    <div class="txt_fb_customer">
                        <p class="txt_fb_cus">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        <p class="name_fb_cus">Stewart Butterfield</p>
                        <p class="pos_fb_cus">CTO &amp; Co-Founder <span>@ Facebook.com</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection