<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/20/16
 * Time: 11:58 PM
 */
?>
@extends('layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset(PATH_ROOT.'assets/css/result.css')}}">
@endsection

@section('content')
    <div class="wrapOuter content-result">
        <div class="bg-service">
            <img class="" src="{{ asset('assets/images/result/img-result-header.png') }}">
        </div>
        <div class="result-notify">
            Kết quả tìm kiếm cho từ <span class="result-text">“Thẻ tín dụng”</span>
        </div>
        <div class="service-wrap-content">
            <div class="service-content">
                <div class="block-serive-content">
                    @if(isset($cards_arr) && count($cards_arr) > 0)
                        @foreach($cards_arr as $card)
                            <div class="block-service-item">
                                <div class="img-card" >
                                    <h4 class="title">{{ $card->ten_the or '' }}</h4>
                                    <img src="{{ PATH_IMAGE.$card->image }}" alt="" class="img_card" style="width: 80%">
                                    <div class="cardContent">
                                        <ul class="starList cf">
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png')}}" alt=""><img src="{{ asset('assets/images/icon_star_off.png')}}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png')}}" alt=""><img src="{{ asset('assets/images/icon_star_off.png')}}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png')}}" alt=""><img src="{{ asset('assets/images/icon_star_off.png')}}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png')}}" alt=""><img src="{{ asset('assets/images/icon_star_off.png')}}" alt=""></li>
                                            <li class="star"><img src="{{ asset('assets/images/icon_star.png')}}" alt=""><img src="{{ asset('assets/images/icon_star_off.png')}}" alt=""></li>
                                        </ul>
                                    </div>
                                    <div class="actionBtnList cf">
                                        <div class="likeBtn text-semibold is-active">Yêu thích</div>
                                        <div class="compareBtn text-semibold text-uppercase is-active">So sánh</div>
                                    </div>
                                </div>
                                <div class="info-card">
                                    <div class="block-border-bottom">
                                        <ul class="infoList block-right-card">
                                            <li class="service-info-text color-1">
                                                <div class="text">{!! $card->khuyen_mai or '' !!}</div>
                                            </li>
                                            <li class="info v01 color-1">
                                                <div class="text">Hạn mức tín dụng: <?php echo isset($card->han_muc_tin_dung) ? number_format($card->han_muc_tin_dung): 0?> VND</div>
                                            </li>
                                            <li class="info v02 color-1">
                                                <div class="text">Yêu cầu thu nhập: <?php echo isset($card->yeu_cau_thu_nhap) ? number_format($card->yeu_cau_thu_nhap) : 0 ?> VND</div>
                                            </li>
                                            <li class="info v03 color-1">
                                                <div class="text">Phí thường niên: <?php echo isset($card->phi_thuong_nien) ? number_format($card->phi_thuong_nien) : 0 ?> VND<br>(Miễn phí năm đầu)</div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="cardFooter block-right-card">
                                        <ul class="btnList cf">
                                            <li class="fl btn v01 text-center text-semibold"><a href="#" class="btnDetail">Xem chi tiết</a></li>
                                            <li class="fr btn v02 text-center text-semibold"><a href="./information.html">Đăng ký ngay</a></li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    @else
                        <div style="margin-left: 40px">Không tìm thấy dữ liệu tương ứng. Trở về <a href="{{config('app.url')}}" style="display: inline"> trang chủ </a></div>
                    @endif
                </div>
            </div>
        </div>
        <div class="service-sidebar">
            <div class="sidebar-content">
                <div class="title-result">
                    Bạn có tìm được<br> mục tiêu mong muốn?
                </div>
                <div class="service-logo">
                    <img src="{{ asset(PATH_ROOT .'assets/images/result/sidebar-img-comokun.png')}}">
                </div>
                <div class="title">
                    Nếu không,<br> hay liên hệ ngay với Comokun:
                </div>
                <button class="btn-result-sidebar">
                    Liên hệ ngay
                </button>
            </div>
            <div class="result-advertise">
                <img src="{{ asset(PATH_ROOT .'assets/images/advertise/advertise.jpg')}}">
            </div>
        </div>
    </div>
@endsection