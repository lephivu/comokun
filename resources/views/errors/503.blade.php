<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/31/16
 * Time: 2:23 AM
 */
?>

@extends('layouts.main')

@section('style')

@endsection

@section('content')
    <div class="body-content outer-top-bd">
        <div class="container">
            <div class="x-page inner-bottom-sm">
                <div class="row">
                    <div class="col-md-12 x-text text-center">
                        <h1>404</h1>
                        <p>Xin lỗi, trang bạn yêu cầu chưa sẵn sàng. </p>
                        <!--form role="form" class="outer-top-vs outer-bottom-xs">
                            <input placeholder="Search" autocomplete="off">
                            <button class="  btn-default le-button">Go</button>
                        </form-->
                        <a href="{{ config('app.url') }}"><i class="fa fa-home"></i> Trở về trang chủ</a>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.sigin-in-->
        </div><!-- /.container -->
    </div><!-- /.body-content -->

@endsection

@section('script')

@endsection