<header class="header container-fluid">
    <nav class="navbar navbar-default navbar-inverse" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" href="{{config('app.url')}}"><img class="img-responsive" src="{{ asset(PATH_ROOT .'assets/images/header_logo.png')}}" width="200" height="32" alt="We will take care about Your finances." /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav main-menu">
                    <li class="mega-menu">
                        <div class="megalv1">
                            <h2><a href="#">Sản phẩm</a></h2>
                        </div>
                        <div class="row megalv2">
                            <div class="home-menu-img-sp">
                                <img src="{{ asset(PATH_ROOT .'assets/images/mega-hover/product-mega.png')}}">
                            </div>
                            <div class="menu-hover-title">
                                Thẻ tín dụng
                            </div>
                            <div class="menu-list-hover">
                                <div class="table-menu-hover">
                                    <div class="item-hover">
                                        <a target="_blank" href="https://www.hsbc.com.vn/1/2/!ut/p/c5/04_SB8K8xLLM9MSSzPy8xBz9CP0os3h3dx9nAwsTA0-DAHcLA0dTQx9ffwMvA4MwA6B8pFm8gbtxqF-ooUGwi4GvR4ineaiZpQEEkKM72NCEFN0Gxp5GIYbulhYGBl4mrm6mxgbE6Q4uy4wP89MvyA0NjSh3VAQAnB2A_w!!/?cid=VNM:TN:P1:CC:01:1803:018:MEGA2-SEM&gclid=EAIaIQobChMI1c-z6LCb2gIVmQgqCh2R5gAUEAAYASAAEgIAm_D_BwE">
                                            <div class="block-menu-hover-img"><img src="{{ asset(PATH_ROOT .'assets/images/mega-hover/hsbc-logo.png')}}" width="120" height="40"></div>
                                                <span>
                                                        thẻ tín dụng<br> HSBC
                                                    </span>
                                        </a>
                                    </div>
                                    <div class="item-hover">
                                        <a href="{{url('category')}}">
                                            <div class="block-menu-hover-img"><img src="{{ asset(PATH_ROOT .'assets/images/mega-hover/standar.png')}}" width="120" height="40"></div>
                                                <span>
                                                        thẻ tín dụng<br>Standard chartered
                                                    </span>
                                        </a>
                                    </div>
                                    <div class="item-hover">
                                        <a href="{{url('category')}}">
                                            <div class="block-menu-hover-img"><img src="{{ asset(PATH_ROOT .'assets/images/mega-hover/citi.png')}}" width="120" height="40"></div>
                                                <span>
                                                        thẻ tín dụng<br>Citi bank
                                                    </span>
                                        </a>
                                    </div>
                                    <div class="item-hover">
                                        <a href="{{url('category')}}">
                                            <div class="block-menu-hover-img"><img src="{{ asset(PATH_ROOT .'assets/images/mega-hover/sacombank.png')}}" width="120" height="40"></div>
                                                <span>
                                                        thẻ tín dụng<br> Sacombank
                                                    </span>
                                        </a>
                                    </div>
                                    <div class="item-hover">
                                        <a href="{{url('category')}}">
                                            <div class="block-menu-hover-img"><img src="{{ asset(PATH_ROOT .'assets/images/mega-hover/vpbank.png')}}" width="120" height="40"></div>
                                                <span>
                                                        thẻ tín dụng<br>VP bank
                                                    </span>
                                        </a>
                                    </div>
                                    <div class="item-hover">
                                        <a href="{{url('category')}}">
                                            <div class="block-menu-hover-img"><img src="{{ asset(PATH_ROOT .'assets/images/mega-hover/acb.png')}}" width="120" height="40"></div>
                                                <span>
                                                        thẻ tín dụng<br>ACB
                                                    </span>
                                        </a>
                                    </div>
                                </div>
                                <button class="see-all-mega" onclick="location.href='{{url('category')}}'">Xem tất cả</button>
                            </div>
                        </div>
                    </li>
                    <li class="mega-menu">
                        <div class="megalv1">
                            <h2><a href="{{ url('promotion') }}">Khuyến mãi</a></h2>
                        </div>
                    </li>
                    <li class="mega-menu">
                        <div class="megalv1">
                            <h2><a href="{{ url('blog') }}">Thư viện</a></h2>
                        </div>
                    </li>
                    <li class="mega-menu">
                        <div class="megalv1">
                            <h2><a href="{{ url('about') }}">Về Comokun</a></h2>
                        </div>
                    </li>
                    <li class="mega-menu">
                        <div class="megalv1">
                            <h2><a href="{{ url('contact') }}">Liên hệ</a></h2>
                        </div>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="login-register">
                        @if(!Auth::user())
                         <a href="#" class="login-button" >Đăng nhập <i class="fa fa-user" aria-hidden="true"></i></a>
                        <div class="table-login-register">
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#login-modal" class="login-button">Đăng nhập</a>
                            <a class=""   data-toggle="modal" data-target="#login-modal" class="register-button" > Tạo tài khoản</a>
                            <a href="{{ url('redirectToProvider/facebook') }}" class="btn-face"><span class="icon-menu"><i class="fa fa-facebook"></i></span>Đăng nhập bằng Facebook</a>
                            {{--<a class="btn-google"><span class="icon-menu"><i class="fa fa-google-plus"></i></span>Đăng nhập bằng Google</a>--}}
                        </div>
                            @else
                                <a href="#"  class="login-button"  style="color: #23527c !important;"> {{Auth::user()->name}} <i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="table-login-register">
                                <a class="btn-sign-out" href="{{ url('logout') }}"><span class="icon-menu"><i class="fa fa-sign-out"></i></span> Đăng xuất</a>
                            </div>
                            @endif
                    </li>
                    <li style="padding-left: 10px">
                        <i class="search fa fa-search search-btn"></i>
                        {!! Form::open(array('id' => "search_results_frm", 'method' => 'POST', 'route' => "result")) !!}
                        <div class="search-open">
                            <div class="input-group animated fadeInDown">
                                <input type="text" class="form-control" placeholder="Tìm kiếm..." name="search_key" value="{{ $search_key or '' }}">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit">Tìm</button>
                        </span>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        {{--<a href="#" class="dropdown-toggle btnSearch01" data-toggle="dropdown"> <i class="fa fa-search" aria-hidden="true"></i></a>--}}
                        {{--<form action="#" method="GET" role="search" class="formSearch cf">--}}
                            {{--<div class="input-group">--}}
                                {{--<input type="text" class="form-control pull-right" placeholder="Search..." name="search">--}}
                                    {{--<span class="input-group-btn">--}}
				                    {{--<button type="reset" class="btn btn-default btnCloseSearch"> <span class="glyphicon glyphicon-remove"> <span class="sr-only">Close</span> </span>--}}
                                    {{--</button>--}}
                                    {{--<button type="submit" class="btn btn-default btnSearch02"> <span class="glyphicon glyphicon-search"> <span class="sr-only">Search</span> </span> </button>--}}
                                    {{--</span>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:;">
    <div class="modal-dialog cf">
        <div class="modalHeader login is-active">
            <div class="modalText">
                <h2 class="modalTitle">Đăng nhập</h2>
                <p class="modalDes">Đăng nhập để tích điểm đổi voucher ưu đãi, lưu lại kết quả đã xem, nhận quà sinh nhật cùng nhiều ưu đãi hấp dẫn.</p>
            </div>
            <img src="images/bg_signin.jpg" alt="" class="maxw100p">
        </div>
        <div class="modalHeader signin">
            <div class="modalText">
                <h2 class="modalTitle">Đăng ký</h2>
                <p class="modalDes">Đăng ký thành viên ngay hôm nay, nhận ngay nhiều ưu đãi hấp dẫn và thông tin hữu ích từ Comokun.</p>
            </div>
            <img src="images/bg_login.jpg" alt="" class="maxw100p">
        </div>
        <div class="modalBody">
            <div class="modalFormArea">
                <div>
                    <ul class="tabHeaderList list-inline">
                        <li tab-header-pc class="tabHeader login is-active"  >Đăng nhập</li>
                        <li tab-header-pc class="tabHeader signin"  >Đăng ký</li>
                    </ul>
                    <ul class="tabList">
                        <li class="login is-active" id="loginPanel">
                            <div tab-body>
                                @include('partials/login_form')
                            </div>
                        </li>
                        <li class="signin" id="registerPanel">
                            <div>
                                @include('partials/register_form')
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
