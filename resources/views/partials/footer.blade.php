<footer class="footer"> <img src="{{ asset(PATH_ROOT .'assets/images/logo_footer.png') }}" alt="" class="logo_footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3 col-xs-12 col-md-offset-2 footerCol col01">
                    <h3>Sản phẩm</h3>
                    <ul>
                        <li><a href="brand.html"><i class="fa fa-chevron-right fa-1" aria-hidden="true"></i> Thẻ tín dụng HSBC</a></li>
                        <li><a href="category.html"><i class="fa fa-chevron-right fa-1" aria-hidden="true"></i> Thẻ tín dụng Citibank</a></li>
                        <li><a href="category.html"><i class="fa fa-chevron-right fa-1" aria-hidden="true"></i> Thẻ tín dụng VPbank</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-12 footerCol">
                    <h3>Về Comokun</h3>
                    <ul>
                        <li><a href="about_us.html"><i class="fa fa-chevron-right fa-1" aria-hidden="true"></i> Về Comokun</a></li>
                        <li><a href="#"><i class="fa fa-chevron-right fa-1" aria-hidden="true"></i> Câu hỏi thường gặp</a></li>
                        <li><a href="#"><i class="fa fa-chevron-right fa-1" aria-hidden="true"></i> Điền khoản sử dụng</a></li>
                        <li><a href="#"><i class="fa fa-chevron-right fa-1" aria-hidden="true"></i> Chính sách bảo mật</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-xs-12 footerCol col3">
                    <h3>Liên hệ</h3>
                    <ul>
                        <li style="line-height: 1.4; padding-right: 5px;"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> Tầng 10, Toà nhà SCETPA, 19A, Cộng Hoà, P.12, Q.Tân Bình, TP. HCM </a></li>
                        <li><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@comokun.vn</a></li>
                        <li><a href="#"><i class="fa fa-headphones" aria-hidden="true"></i> + 84 28 7302 4248</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-9 col-md-push-3 col-xs-12 socialLink">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/comokun/"><img src="{{ asset(PATH_ROOT .'assets/images/ico_facebook.jpg')}}" width="44" height="37" alt="" /></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{ asset(PATH_ROOT .'assets/images/ico_twitter.jpg')}}" width="44" height="37" alt="" /></a>
                        </li>
                        <li>
                            <a href="#"><img src="{{ asset(PATH_ROOT .'assets/images/ico_google.jpg')}}" width="44" height="37" alt="" /></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <a href="#top" class="toTop"><img src="{{ asset(PATH_ROOT .'assets/images/page_top.png')}}" width="57" height="57" alt="" /></a>
    </div>
</footer>