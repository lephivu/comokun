@if(isset($successMsg) && !empty($successMsg))
    <div class="alert alert-success fade in m-b-15">
        {{ $successMsg }}
        <span class="close" data-dismiss="alert">×</span>
    </div>
@endif
@if(isset($errorMsg) && !empty($errorMsg))
<div class="alert alert-danger fade in m-b-15">
    <strong>Lỗi!</strong>
    {{ $errorMsg }}
    <span class="close" data-dismiss="alert">×</span>
</div>
@endif

{!! Form::open(array('id' => "register_frm", 'class' =>'myForm', 'method'=>'POST', 'route' => "register")) !!}

<div class="formGroup">
    <p class="label">Họ Tên</p>
    <div class="field"><input type="text" name="fullname" placeholder="* Nhập họ tên" class="input" value="{{ isset($customer) ? $customer->name :'' }}"></div>
</div>
<div class="formGroup">
    <p class="label">Email</p>
    <div class="field"><input type="text" name="email" placeholder="* Nhập Email" class="input" value="{{ isset($customer) ? $customer->email :'' }}" ></div>
</div>
<div class="formGroup">
    <p class="label">SĐT</p>
    <div class="field"><input type="text" name="phone" placeholder="* Nhập số điện thoại" class="input" value="{{ isset($customer) ? $customer->mobile_phone :'' }}" ></div>
</div>
<div class="formGroup">
    <p class="label">Tên đăng nhập</p>
    <div class="field"><input type="text" name="username" placeholder="* Tên đăng nhập" class="input" value="{{ isset($customer) ? $customer->username :'' }}"></div>
</div>
<div class="formGroup">
    <p class="label">Mật khẩu</p>
    <div class="field"><input type="password" name="password" placeholder="* Mật khẩu từ 6 đến 32 ký tự" class="input"></div>
</div>
<div class="formGroup mtx mbx">
    <p class="label ptx">Giới tính</p>
    <div class="field">
        <label><input type="radio" name="gender" class="radio" value="1"><span>Nam</span></label>
        <label><input type="radio" name="gender" class="radio" value="0"><span>Nữ</span></label>
    </div>
</div>
{{--<div class="formGroup">--}}
{{--<p class="label">Sinh nhật</p>--}}
{{--<div class="field">--}}
{{--<select class="select">--}}
{{--<option value="">Ngày</option>--}}
{{--</select>--}}
{{--<select class="select">--}}
{{--<option value="">Tháng</option>--}}
{{--</select>--}}
{{--<select class="select">--}}
{{--<option value="">Năm</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--</div>--}}
<button type="submit" id="btn-register" class="btnSubmit"> Đăng ký</button>
{!! Form::close() !!}