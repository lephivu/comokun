<header class="header container-fluid">
    <nav class="navbar navbar-default navbar-inverse" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <h1>
                    <a class="navbar-brand" href="{{config('app.url')}}"><img class="img-responsive" src="{{ asset(PATH_ROOT .'assets/images/header_logo.png')}}" width="279" height="32" alt="" /></a>
                </h1>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="select_desire">
                    <select name="">
                        <option value="">Nhu cầu sử dụng thẻ tín dụng của bạn là:</option>
                        <option value="1">
                            Mua sắm và giải trí
                        </option>
                        <option value="1">
                            Mua sắm trả góp
                        </option>
                        <option value="1">
                            Công tác, du lịch & tích lũy dặm bay
                        </option>
                        <option value="1">
                            Hoàn tiền
                        </option>
                        <option value="1">
                            Trả phí bảo hiểm
                        </option>
                        <option value="1">
                            Di chuyển hàng ngày
                        </option>
                    </select>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li class="login-register">
                        @if(!Auth::user())
                            <a href="#" class="login-button" >Đăng nhập <i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="table-login-register">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#login-modal" class="login-button">Đăng nhập</a>
                                <a class=""   data-toggle="modal" data-target="#login-modal" class="register-button" > Tạo tài khoản</a>
                                <a href="{{ url('redirectToProvider/facebook') }}" class="btn-face"><span class="icon-menu"><i class="fa fa-facebook"></i></span>Đăng nhập bằng Facebook</a>
                                {{--<a class="btn-google"><span class="icon-menu"><i class="fa fa-google-plus"></i></span>Đăng nhập bằng Google</a>--}}
                            </div>
                        @else
                            <a href="#"  class="login-button"  style="color: #23527c !important;"> {{Auth::user()->name}} <i class="fa fa-user" aria-hidden="true"></i></a>
                            <div class="table-login-register">
                                <a class="btn-sign-out" href="{{ url('logout') }}"><span class="icon-menu"><i class="fa fa-sign-out"></i></span> Đăng xuất</a>
                            </div>
                        @endif
                    </li>
                    <li style="padding-left: 10px">
                        <i class="search fa fa-search search-btn"></i>
                        {!! Form::open(array('id' => "search_results_frm", 'method' => 'POST', 'route' => "result")) !!}
                        <div class="search-open">
                            <div class="input-group animated fadeInDown">
                                <input type="text" class="form-control" placeholder="Tìm kiếm..." name="search_key" value="{{ $search_key or '' }}">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit">Tìm</button>
                        </span>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:;">
    <div class="modal-dialog cf">
        <div class="modalHeader login is-active">
            <div class="modalText">
                <h2 class="modalTitle">Đăng nhập</h2>
                <p class="modalDes">Đăng nhập để tích điểm đổi voucher ưu đãi, lưu lại kết quả đã xem, nhận quà sinh nhật cùng nhiều ưu đãi hấp dẫn.</p>
            </div>
            <img src="images/bg_signin.jpg" alt="" class="maxw100p">
        </div>
        <div class="modalHeader signin">
            <div class="modalText">
                <h2 class="modalTitle">Đăng ký</h2>
                <p class="modalDes">Đăng ký thành viên ngay hôm nay, nhận ngay nhiều ưu đãi hấp dẫn và thông tin hữu ích từ Comokun.</p>
            </div>
            <img src="images/bg_login.jpg" alt="" class="maxw100p">
        </div>
        <div class="modalBody">
            <div class="modalFormArea">
                <div>
                    <ul class="tabHeaderList list-inline">
                        <li tab-header-pc class="tabHeader login is-active"  >Đăng nhập</li>
                        <li tab-header-pc class="tabHeader signin"  >Đăng ký</li>
                    </ul>
                    <ul class="tabList">
                        <li class="login is-active" id="loginPanel">
                            <div tab-body>
                                @include('partials/login_form')
                            </div>
                        </li>
                        <li class="signin" id="registerPanel">
                            <div>
                                @include('partials/register_form')
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
