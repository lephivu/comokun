<?php
   for($i =0; $i < 4; $i++){
          ?>

<li class="item-product-compare">
    <div class="block-item-product <?php echo (isset($card_arr[$i]) && !empty($card_arr[$i]->image)) ? 'is-active' : '' ?>">
        <div class="info-item-compare">
            <div class="item-default"><i class="fa fa-credit-card" aria-hidden="true"></i></div>
            <img src="<?php echo (isset($card_arr[$i]) && !empty($card_arr[$i]->image)) ? PATH_IMAGE.$card_arr[$i]->image : '' ?> " width="96" height="60" alt="" class="">
        </div>
        @if(isset($card_arr[$i]) && $card_arr[$i]->id > 0)
        <div class="close_compare" data-href-remove="{{ url('comparison_remove/'.$card_arr[$i]->id) }}">
            <i class="fa fa-close"></i>
        </div>
            @endif
    </div>
</li>

          <?php
   }


?>


<script>
    $('.close_compare').click(function() {
        //$(this).closest('.list-product-compare li .block-item-product').removeClass("is-active");
        var removeURL = $(this).attr('data-href-remove');
        $('.list-product-compare').load(removeURL, function(){

        });
    });
</script>
