
@if(isset($errorMsg) && !empty($errorMsg))
<div class="alert alert-danger fade in m-b-15">
    <strong>Lỗi!</strong>
    {{ $errorMsg }}
    <span class="close" data-dismiss="alert">×</span>
</div>
@endif

{!! Form::open(array('id' => "login_frm", 'class' =>'myForm', 'method'=>'POST', 'route' => "login")) !!}
<div class="formGroup">
    <p class="label">Tên đăng nhập</p>
    <div class="field"><input type="text" name="username" placeholder="Nhập tên đăng nhập" class="input"></div>
</div>
<div class="formGroup">
    <p class="label">Mật khẩu</p>
    <div class="field"><input type="text" name="password" placeholder="Mật khẩu từ 6 đến 32 ký tự" class="input"></div>
</div>
<div class="formGroup">
    <div class="field forget">Quên mật khẩu? Nhấn vào <a href="#">đây</a></div>
</div>
<button type="submit" id="btn-login" class="btnSubmit">Đăng nhập</button>
<a href="{{ url('redirectToProvider/facebook') }}" id="btnLoginFacebook" class="btnLoginFacebook"><span class="iconLoginFacebook"><img src="images/icon_login_facebook.png" alt=""></span>Đăng nhập bằng Facebook</a>
{{--<button class="g-signin"--}}
        {{--data-scope="email"--}}
        {{--data-clientid="217542344558-qadj9tacru43ij5v9dodi260fdjq8q7g.apps.googleusercontent.com"--}}
        {{--data-theme="dark"--}}
        {{--data-cookiepolicy="single_host_origin">--}}
    {{--<span class="iconLoginGoogle"><img src="images/icon_login_google.png" alt=""></span>--}}
    {{--Đăng nhập bằng Google--}}
{{--</button>--}}
{!! Form::close() !!}
