<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/31/16
 * Time: 2:23 AM
 */
?>

@extends('layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/promotion.css') }}">
@endsection

@section('content')
    <div class="img-promotion-header">
        <img src="{{ asset('assets/images/promotion/banner-promotion.jpg') }}">
        <!--<img src="./images/promotion/img-promotion-header.jpg">-->
    </div>
    <div class="wrapOuter promotion-img-content">
        <div class="container">
            <div class="block-promotion-img">
                <a href="{{ asset('assets/images/event/img-event-1.jpg') }}" target="_blank"><img src="{{ asset('assets/images/promotion/img-1.jpg') }}"></a>
            </div>
            <div class="block-promotion-img">
                <a href="{{ asset('assets/images/event/img-event-2.jpg') }}" target="_blank"><img src="{{ asset('assets/images/promotion/img-2.jpg') }}"></a>
            </div>
        </div>
    </div>
    <div class="block-send-email">
        <div class="newsletter">
            <div class="ttl_newsletter">
                <p class="img_newsletter"><img src="{{ asset('assets/images/page-account/newsletter.png') }}" alt=""></p>
                <p class="txt_newsletter">
                    <span class="fs24">Đăng ký nhận tin</span>
                    <span class="fs24"> từ Comokun.vn</span>
                </p>
            </div>
            <div class="form_newletter">
                <div class="form_newletterArea">
                    <form action="#" method="post">
                        <input type="text" placeholder="Nhập Email của bạn" class="txtEmail">
                        <input type="submit" value="XÁC NHẬN" class="newsletter_submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection