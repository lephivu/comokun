/*
 Navicat Premium Data Transfer

 Source Server         : Transcosmos - Comokun
 Source Server Type    : MySQL
 Source Server Version : 50505
 Source Host           : localhost
 Source Database       : comokun

 Target Server Type    : MySQL
 Target Server Version : 50505
 File Encoding         : utf-8

 Date: 03/31/2018 00:03:45 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `customers`
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_assci` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `tax_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_passport` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tinh_thanh_id` int(10) unsigned DEFAULT NULL,
  `quan_huyen_id` int(10) unsigned DEFAULT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`),
  KEY `customers_tinh_thanh_id_foreign` (`tinh_thanh_id`),
  KEY `customers_quan_huyen_id_foreign` (`quan_huyen_id`),
  CONSTRAINT `customers_quan_huyen_id_foreign` FOREIGN KEY (`quan_huyen_id`) REFERENCES `quan_huyen` (`id`),
  CONSTRAINT `customers_tinh_thanh_id_foreign` FOREIGN KEY (`tinh_thanh_id`) REFERENCES `tinh_thanh` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `doi_tac`
-- ----------------------------
DROP TABLE IF EXISTS `doi_tac`;
CREATE TABLE `doi_tac` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loai_doi_tac_id` int(10) unsigned NOT NULL,
  `ten_doi_tac` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chi_tiet` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `doi_tac_loai_doi_tac_id_foreign` (`loai_doi_tac_id`),
  CONSTRAINT `doi_tac_loai_doi_tac_id_foreign` FOREIGN KEY (`loai_doi_tac_id`) REFERENCES `loai_doi_tac` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `doi_tac`
-- ----------------------------
BEGIN;
INSERT INTO `doi_tac` VALUES ('1', '6', 'Vietnam Airline', '1522380156.png', '<b></b>Tặng 1.500 dặm bay cho chủ thẻ HSBC khi&nbsp; đăng ký thành công hội viên Bông Sen Vàng Thời gian áp dụng&nbsp; đến 29/12/2018', '1'), ('2', '8', 'Anam QT Spa', '1522380246.jpg', 'Giảm giá 10% trên tổng hóa đơn cho chủ thẻ tín dụng Visa Chuẩn.<br>\r\nThời gian hiệu lực đến 14/04/2018', '1'), ('3', '9', 'The Coffee Bean & Tea Leaf', '1522380284.png', 'Giảm 10% bill các ngày trong tuần cho chue thẻ.<br>\r\nThời gian áp dụng:  <br>\r\n. Hồ Chí Minh: 19/01/2018 – 29/07/2018<br>\r\n. Hà Nội: 26/01/2018 – 29/07/2018', '1');
COMMIT;

-- ----------------------------
--  Table structure for `doi_tac_the`
-- ----------------------------
DROP TABLE IF EXISTS `doi_tac_the`;
CREATE TABLE `doi_tac_the` (
  `the_id` int(10) unsigned NOT NULL,
  `doi_tac_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`the_id`,`doi_tac_id`),
  KEY `doi_tac_the_doi_tac_id_foreign` (`doi_tac_id`),
  CONSTRAINT `doi_tac_the_doi_tac_id_foreign` FOREIGN KEY (`doi_tac_id`) REFERENCES `doi_tac` (`id`),
  CONSTRAINT `doi_tac_the_the_id_foreign` FOREIGN KEY (`the_id`) REFERENCES `the` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `doi_tac_the`
-- ----------------------------
BEGIN;
INSERT INTO `doi_tac_the` VALUES ('1', '1'), ('1', '2'), ('1', '3');
COMMIT;

-- ----------------------------
--  Table structure for `loai_doi_tac`
-- ----------------------------
DROP TABLE IF EXISTS `loai_doi_tac`;
CREATE TABLE `loai_doi_tac` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loai_doi_tac_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `loai_doi_tac`
-- ----------------------------
BEGIN;
INSERT INTO `loai_doi_tac` VALUES ('6', 'mua-sam', 'Mua sắm', '1'), ('7', 'du-lich', 'Du lịch', '1'), ('8', 'suc-khoe', 'Sức khỏe', '1'), ('9', 'an-uong', 'Ăn uống', '1');
COMMIT;

-- ----------------------------
--  Table structure for `loai_the`
-- ----------------------------
DROP TABLE IF EXISTS `loai_the`;
CREATE TABLE `loai_the` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ten_loai` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loai_the_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `loai_the`
-- ----------------------------
BEGIN;
INSERT INTO `loai_the` VALUES ('1', 'Visa', 'visa', '1'), ('2', 'Amex', 'amex', '1'), ('3', 'Master Card', 'master-card', '1'), ('4', 'Union Pay', 'union-pay', '1'), ('5', 'JCB', 'jcb', '1');
COMMIT;

-- ----------------------------
--  Table structure for `media`
-- ----------------------------
DROP TABLE IF EXISTS `media`;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_slug_unique` (`slug`),
  KEY `media_user_id_foreign` (`user_id`),
  CONSTRAINT `media_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1'), ('2', '2014_10_12_100000_create_password_resets_table', '1'), ('3', '2018_03_10_155315_create_loai_the_table', '1'), ('4', '2018_03_10_155420_create_ngan_hang_table', '1'), ('5', '2018_03_10_155625_create_loai_doi_tac_table', '1'), ('6', '2018_03_10_155828_create_tinh_thanh_table', '1'), ('7', '2018_03_10_155904_create_quan_huyen_table', '1'), ('8', '2018_03_11_154802_create_customers_table', '1'), ('9', '2018_03_11_154912_create_the_table', '1'), ('10', '2018_03_11_154941_create_uu_dai_table', '1'), ('11', '2018_03_11_155009_create_uu_dai_the_table', '1'), ('12', '2018_03_11_155518_create_doi_tac_table', '1'), ('13', '2018_03_11_155551_create_doi_tac_the_table', '1'), ('14', '2018_03_27_200028_create_media_table', '1');
COMMIT;

-- ----------------------------
--  Table structure for `ngan_hang`
-- ----------------------------
DROP TABLE IF EXISTS `ngan_hang`;
CREATE TABLE `ngan_hang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ten_ngan_hang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `html` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `landing_page` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ngan_hang_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=638 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `ngan_hang`
-- ----------------------------
BEGIN;
INSERT INTO `ngan_hang` VALUES ('1', 'HSBC', 'hsbc', '1522379247.png', 'Ngân hàng TNHH một thành viên HSBC (Việt Nam)', '0', '1'), ('2', 'Citibank', 'slugcitibank', 'test.jpg', '', '0', '1'), ('3', 'ANZ', 'sluganz', 'test.jpg', '', '0', '1'), ('4', 'Sacombank', 'slugsacombank', 'test.jpg', '', '0', '1'), ('5', 'Standard Chartered', 'slugstandardchartered', 'test.jpg', '', '0', '1'), ('6', 'Vietinbank', 'slugvietinbank', 'test.jpg', '', '0', '1'), ('7', 'Vietcombank', 'slugvietcombank', 'test.jpg', '', '0', '1'), ('8', 'BIDV', 'slugbidv', 'test.jpg', '', '0', '1'), ('9', 'Techcombank', 'slugtechcombank', 'test.jpg', '', '0', '1'), ('10', 'VPBank', 'slugvpbank', 'test.jpg', '', '0', '1'), ('11', 'Maritime Bank', 'slugmaritime bank', 'test.jpg', '', '0', '1'), ('12', 'DongA Bank', 'slugdonga bank', 'test.jpg', '', '0', '1'), ('13', 'EximBank', 'slugeximbank', 'test.jpg', '', '0', '1'), ('14', 'Nam A Bank', 'slugnam a bank', 'test.jpg', '', '0', '1'), ('15', 'Saigonbank', 'slugsaigonbank', 'test.jpg', '', '0', '1'), ('16', 'MBBank', 'slugmbbank', 'test.jpg', '', '0', '1'), ('17', 'VIB', 'slugvib', 'test.jpg', '', '0', '1'), ('18', 'SeABank', 'slugseabank', 'test.jpg', '', '0', '1'), ('19', 'HDBank', 'slughdbank', 'test.jpg', '', '0', '1'), ('20', 'Viet Capital Bank', 'slugviet capital bank', 'test.jpg', '', '0', '1'), ('21', 'OCB', 'slugocb', 'test.jpg', '', '0', '1'), ('22', 'SCB', 'slugscb', 'test.jpg', '', '0', '1'), ('23', 'SHB', 'slugshb', 'test.jpg', '', '0', '1'), ('24', 'ABBank', 'slugabbank', 'test.jpg', '', '0', '1'), ('25', 'NCB', 'slugncb', 'test.jpg', '', '0', '1'), ('26', 'KienLong Bank', 'slugkienlong bank', 'test.jpg', '', '0', '1'), ('27', 'OceanBank', 'slugoceanbank', 'test.jpg', '', '0', '1'), ('28', 'PG Bank', 'slugpgbank', 'test.jpg', '', '0', '1'), ('29', 'CBBank', 'slugcbbank', 'test.jpg', '', '0', '1'), ('30', 'LienVietPostBank', 'sluglienvietpostbank', 'test.jpg', '', '0', '1'), ('31', 'TPBank', 'slugtpbank', 'test.jpg', '', '0', '1'), ('32', 'BAOVIET Bank', 'slugbaoviet bank', 'test.jpg', '', '0', '1'), ('33', 'Agribank', 'slugagribank', 'test.jpg', '', '0', '1'), ('34', 'Indovina Bank', 'slugindovina bank', 'test.jpg', '', '0', '1'), ('35', 'VRB', 'slugvrb', 'test.jpg', '', '0', '1'), ('36', 'Shinhan Bank', 'slugshinhan bank', 'test.jpg', '', '0', '1'), ('37', 'JACCS', 'slugjaccs', 'test.jpg', '', '0', '1'), ('38', 'PVcomBank', 'slugpvcombank', 'test.jpg', '', '0', '1'), ('39', 'ACB', 'slugacb', 'test.jpg', '', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `quan_huyen`
-- ----------------------------
DROP TABLE IF EXISTS `quan_huyen`;
CREATE TABLE `quan_huyen` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tinh_thanh_id` int(10) unsigned NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `quan_huyen_tinh_thanh_id_foreign` (`tinh_thanh_id`),
  CONSTRAINT `quan_huyen_tinh_thanh_id_foreign` FOREIGN KEY (`tinh_thanh_id`) REFERENCES `tinh_thanh` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `the`
-- ----------------------------
DROP TABLE IF EXISTS `the`;
CREATE TABLE `the` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `user_edit` int(10) unsigned DEFAULT NULL,
  `ten_the` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ngan_hang_id` int(10) unsigned NOT NULL,
  `loai_the_id` int(10) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lai_suat` double(4,2) NOT NULL,
  `so_ngay_an_han` double(5,2) NOT NULL,
  `hoan_tien_trung_binh` double(4,2) NOT NULL,
  `hoan_tien_cao_nhat` double(4,2) NOT NULL,
  `yeu_cau_thu_nhap` double(12,2) NOT NULL,
  `hinh_thuc_nhan_luong` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `han_muc_tin_dung` double(16,2) NOT NULL,
  `han_muc_rut_tien_mat` double(16,2) NOT NULL,
  `phi_phat_hanh` double(12,2) NOT NULL,
  `phi_thuong_nien` double(12,2) NOT NULL,
  `phi_ung_tien_mat` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phi_cham_tra_no_tin_dung` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phi_giao_dich_ngoai_te` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phi_duy_tri_the_phu` double(12,2) NOT NULL,
  `so_tien_toi_thieu_tra_hang_thang` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tra_gop_lai_suat_0` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `uu_dai_di_uber_grab` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wireless_payment` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `bao_hiem_du_lich` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ho_tro_du_lich_247` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `mien_phi_phong_cho_san_bay` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `hoan_tien_khi_tra_phi_bao_hiem` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `khach_hang_than_thiet` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chi_tieu_tren_mot_dam_bay` double(12,2) DEFAULT NULL,
  `khuyen_mai` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `khuyen_mai_mo_the` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chung_minh_nhan_than` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chung_minh_cu_tru` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chung_minh_thu_nhap` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `the_doi_the` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `thu_ho_so_tai_nha` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `yeu_cau_khac` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` int(10) unsigned NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `the_slug_unique` (`slug`),
  KEY `the_user_id_foreign` (`user_id`),
  KEY `the_user_edit_foreign` (`user_edit`),
  KEY `the_ngan_hang_id_foreign` (`ngan_hang_id`),
  KEY `the_loai_the_id_foreign` (`loai_the_id`),
  CONSTRAINT `the_loai_the_id_foreign` FOREIGN KEY (`loai_the_id`) REFERENCES `loai_the` (`id`),
  CONSTRAINT `the_ngan_hang_id_foreign` FOREIGN KEY (`ngan_hang_id`) REFERENCES `ngan_hang` (`id`),
  CONSTRAINT `the_user_edit_foreign` FOREIGN KEY (`user_edit`) REFERENCES `users` (`id`),
  CONSTRAINT `the_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `the`
-- ----------------------------
BEGIN;
INSERT INTO `the` VALUES ('1', '2', '2', 'Thẻ Tín dụng Visa Chuẩn', 'hsbc-visa-chuan', '1', '1', 'visa-chuan.png', '2.60', '45.00', '0.30', '2.00', '6000000.00', '1', '60000000.00', '65.00', '0.00', '350000.00', '4% (tối thiểu là 50.000đ)', '4% khoản nợ tối thiêu( tối thiểu \r\n80.000đ tối đa 630.000đ', '3.49%/ giá trị mỗi giao dịch', '250000.00', '5% của số dư nợ (tối thiểu 50.000 VND)', 'trả góp 0% tại các đối tác như Nguyễn kim,\r\n Lazada, FPTshop, Samsung,\r\n Điện máy Chợ Lớn, Thiên Hòa, Icenter,....', 'không áp dụng', '1', '0', '0', '0', '0', 'HSBC Home& Away', '45000.00', 'My Chicken Run: Giảm 10% trên tổng hóa đơn<br>Nhà hàng O’Learys: Giảm 10% trên tổng hóa đơn.<br>Fanny Ice Cream: Giảm 10% trên tổng hóa đơn.<br>Nước hoa Paris Lolita Lempicka: Giảm 10% trên tổng hóa đơn.<br>Thời trang Vascara: Giảm 5% trên tổng hóa đơn.', 'Hoàn tiền 1 triệu VND khi mở \r\nThẻ tín dụng HSBC Visa Chuẩn và \r\nchi tiêu tối thiểu 2 triệu VND trong\r\n 60 ngày.', 'bản sao Chứng Minh Nhân Dân\r\nCăn Cước Công Dân/Hộ Chiếu', 'Hộ khẩu hoặc giấy đăng ký tạm trú, bằng lái xe\r\n còn hiệu lực.\r\nHóa đơn điện, nước, internet hoặc điện thoại \r\ntrong những tháng gần nhất', 'Hợp đồng lao động, Sao kê 3 tháng lương gần nhất', '1', '1', null, '10', '1', '2018-03-30 03:45:17', '2018-03-30 06:59:00');
COMMIT;

-- ----------------------------
--  Table structure for `tich_luy_uu_dai`
-- ----------------------------
DROP TABLE IF EXISTS `tich_luy_uu_dai`;
CREATE TABLE `tich_luy_uu_dai` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tich_luy_uu_dai_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `tich_luy_uu_dai`
-- ----------------------------
BEGIN;
INSERT INTO `tich_luy_uu_dai` VALUES ('1', 'dặm bay', 'dam-bay', '1');
COMMIT;

-- ----------------------------
--  Table structure for `tich_luy_uu_dai_the`
-- ----------------------------
DROP TABLE IF EXISTS `tich_luy_uu_dai_the`;
CREATE TABLE `tich_luy_uu_dai_the` (
  `tich_luy_uu_dai_id` int(10) unsigned NOT NULL,
  `the_id` int(10) unsigned NOT NULL,
  KEY `tich_luy_uu_dai_the_tich_luy_uu_dai_id_foreign` (`tich_luy_uu_dai_id`),
  KEY `tich_luy_uu_dai_the_the_id_foreign` (`the_id`),
  CONSTRAINT `tich_luy_uu_dai_the_the_id_foreign` FOREIGN KEY (`the_id`) REFERENCES `the` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tich_luy_uu_dai_the_tich_luy_uu_dai_id_foreign` FOREIGN KEY (`tich_luy_uu_dai_id`) REFERENCES `tich_luy_uu_dai` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `tinh_thanh`
-- ----------------------------
DROP TABLE IF EXISTS `tinh_thanh`;
CREATE TABLE `tinh_thanh` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tinh_thanh_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', '1', 'Tuan Tran', 'splus.codes@gmail.com', '$2y$10$m/68IVPEQF7w3pd9VxMqfOaED4omPBkv2XvLs7yBBaIMoXApkOJSK', 'f361aa8400d420b55faa21097de0a54d.jpg', 'EYlojg0mc1VdCKfz5a4gR8RrBBUvR3kFd2ZitgznnYGWaZrUhhGBUbKP7pEA', null, '1', '2018-03-30 01:53:09', '2018-03-30 01:53:09'), ('2', '1', 'Vu Le', 'vu.lp@trans-cosmos.com.vn', '$2y$10$aM3Do9Mu4ps5DM3MA4mOBur0T2W.P0Zp.Gk8Bu9PLAci27SWtG9Em', '55c7708b2397a15f4d0a86e43e6639f5.jpg', null, null, '1', '2018-03-30 01:53:10', '2018-03-30 01:53:10');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
