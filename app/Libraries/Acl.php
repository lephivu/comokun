<?php

/**
 * Create authorizing defitions. There are 5 user types: Super Manager, Manager, Supervisor, Leader, Agent
 * Contains functions relative to authorization
 *
 * @author                   Dung Tao
 * @created date             02 May 2016
 * @since                    Version 1.0
 */
namespace App\Libraries {

    use Illuminate\Support\Facades\Auth;

    class Acl
    {

        var $_SUPERMAN = 5;
        var $_MANAGER = 4;
        var $_SUPERVISOR = 3;
        var $_LEADER = 2;
        var $_AGENT = 1;

        public function get_superman_type()
        {
            return $this->_SUPERMAN;
        }

        public function get_manager_type()
        {
            return $this->_MANAGER;
        }

        public function get_supervisor_type()
        {
            return $this->_SUPERVISOR;
        }

        public function get_leader_type()
        {
            return $this->_LEADER;
        }

        public function get_agent_type()
        {
            return $this->_AGENT;
        }

        /**
         * check whether is super manager with given user type or level in session
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    string p_u_type : given user type (optional)
         * @return boolean true is super manager, otherwise false
         */
        public function is_super_man($p_u_type = '')
        {
            if ($p_u_type == '') $p_u_type = Auth::user()->level;
            return $p_u_type != "" && $p_u_type == $this->_SUPERMAN;
        }

        /**
         * check whether is manager with given user type or level in session
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    string p_u_type : given user type (optional)
         * @return boolean true is manager, otherwise false
         */
        public function is_manager($p_u_type = '')
        {
            if ($p_u_type == '') $p_u_type = Auth::user()->level;
            return $p_u_type != "" && $p_u_type == $this->_MANAGER;
        }

        /**
         * check whether is supervisor with given user type or level in session
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    string p_u_type : given user type (optional)
         * @return boolean true is supervisor, otherwise false
         */
        public function is_supervisor($p_u_type = '')
        {
            if ($p_u_type == '') $p_u_type = Auth::user()->level;
            return $p_u_type != "" && $p_u_type == $this->_SUPERVISOR;
        }

        /**
         * check whether is leader with given user type or level in session
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    string p_u_type : given user type (optional)
         * @return boolean true is leader, otherwise false
         */
        public function is_leader($p_u_type = '')
        {
            if ($p_u_type == '') $p_u_type =Auth::user()->level;
            return $p_u_type != "" && $p_u_type == $this->_LEADER;
        }

        /**
         * check whether is agent with given user type or level in session
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    string p_u_type : given user type (optional)
         * @return boolean true is agent, otherwise false
         */
        public function is_agent($p_u_type = '')
        {
            if ($p_u_type == '') $p_u_type = Auth::user()->level;
            return $p_u_type != "" && $p_u_type == $this->_AGENT;
        }

        /**
         * get name of given user type
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    string p_u_type : given user type
         * @return string name of user type code
         */
        public function get_user_type_name($p_u_type)
        {
            if ($p_u_type == $this->_SUPERMAN)
                return 'Admin';
            else if ($p_u_type == $this->_MANAGER)
                return 'Manager';
            else if ($p_u_type == $this->_SUPERVISOR)
                return 'Supervisor';
            else if ($p_u_type == $this->_LEADER)
                return 'Leader';
            else if ($p_u_type == $this->_AGENT)
                return 'Agent';
        }

        public function get_user_type_name_label($p_u_type)
        {
            if ($p_u_type == $this->_SUPERMAN)
                return '<span class="label label-sm label-purple span-60">Admin</span>';
            else if ($p_u_type == $this->_MANAGER)
                return '<span class="label label-sm label-primary span-60">Manager</span>';
            else if ($p_u_type == $this->_SUPERVISOR)
                return '<span class="label label-sm label-primary span-60">Supervisor</span>';
            else if ($p_u_type == $this->_LEADER)
                return '<span class="label label-sm label-success span-60">Leader</span>';
            else if ($p_u_type == $this->_AGENT)
                return '<span class="label label-sm label-default span-60">Agent</span>';
        }

        public function get_user_type_name_short($p_u_type)
        {
            if ($p_u_type == $this->_SUPERMAN)
                return 'Admin';
            else if ($p_u_type == $this->_MANAGER)
                return 'Manager';
            else if ($p_u_type == $this->_SUPERVISOR)
                return 'SUP';
            else if ($p_u_type == $this->_LEADER)
                return 'TL';
            else if ($p_u_type == $this->_AGENT)
                return 'Agent';

        }


        public function get_roles_arr()
        {
            $roles_arr = array(
                $this->_AGENT => $this->get_user_type_name($this->_AGENT),
            );
            if ($this->is_supervisor() || $this->is_manager() || $this->is_super_man()) {
                $roles_arr[$this->_LEADER] = $this->get_user_type_name($this->_LEADER);
            }
            if ($this->is_manager() || $this->is_super_man()) {
                $roles_arr[$this->_SUPERVISOR] = $this->get_user_type_name($this->_SUPERVISOR);
            }
            if ($this->is_super_man()) {
                $roles_arr[$this->_MANAGER] = $this->get_user_type_name($this->_MANAGER);
            }
            return $roles_arr;
        }

        /**
         * make combobox of user type
         *   Super Manager can see all types of user (MANAGER, SUPERVISOR, LEADER, AGENT)
         *   Manager can see SUPERVISOR, LEADER, AGENT
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    string p_u_type : given user type (optional)
         * @return string HTML code for putting between the tag "<select></select>" to render an HTML combobox
         */
        public function make_u_type_combobox($p_user_type = "")
        {
            $res_str = "";

            if ($this->is_super_man()) {
                $select_str = $p_user_type == $this->_AGENT ? "selected" : "";
                $res_str .= "<option value='" . $this->_AGENT . "' $select_str> " . $this->get_user_type_name($this->_AGENT) . " </option> \n";
                $select_str = $p_user_type == $this->_LEADER ? "selected" : "";
                $res_str .= "<option value='" . $this->_LEADER . "' $select_str> " . $this->get_user_type_name($this->_LEADER) . " </option> \n";
                $select_str = $p_user_type == $this->_SUPERVISOR ? "selected" : "";
                $res_str .= "<option value='" . $this->_SUPERVISOR . "' $select_str> " . $this->get_user_type_name($this->_SUPERVISOR) . " </option> \n";
                $select_str = $p_user_type == $this->_MANAGER ? "selected" : "";
                $res_str .= "<option value='" . $this->_MANAGER . "' $select_str> " . $this->get_user_type_name($this->_MANAGER) . " </option> \n";
            } else if ($this->is_manager()) {
                $select_str = $p_user_type == $this->_AGENT ? "selected" : "";
                $res_str .= "<option value='" . $this->_AGENT . "' $select_str> " . $this->get_user_type_name($this->_AGENT) . " </option> \n";
                $select_str = $p_user_type == $this->_LEADER ? "selected" : "";
                $res_str .= "<option value='" . $this->_LEADER . "' $select_str> " . $this->get_user_type_name($this->_LEADER) . " </option> \n";
                $select_str = $p_user_type == $this->_SUPERVISOR ? "selected" : "";
                $res_str .= "<option value='" . $this->_SUPERVISOR . "' $select_str> " . $this->get_user_type_name($this->_SUPERVISOR) . " </option> \n";
            }

            return $res_str;
        }

        /**
         * get owning rooms of login user
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @return string ids of room separated by comma (ex: "1,3,4,5")
         */

        public function get_owning_team_ids()
        {
            $ci = get_instance();
            $ci->load->model("team_model");

            if ($this->is_super_man() || $this->is_manager()) {

                $result = $ci->team_model->find_all_active();
                $room_ids = array();
                for ($i = 0; $i < count($result); $i++) {
                    $room_ids[] = $result[$i]["id"];
                }
                return implode(',', $room_ids);
            } else if ($this->is_supervisor()) {
                $result = $ci->team_model->get_by_field(array('department_id' => $ci->session->userdata("department_id")));
                $room_ids = array();
                for ($i = 0; $i < count($result); $i++) {
                    $room_ids[] = $result[$i]["id"];
                }
                return implode(',', $room_ids);
            }

            return implode(",", $ci->session->userdata("sess_team_ids"));
        }

        public function make_owning_team_combobox()
        {
            $ci = get_instance();
            $ci->load->model("team_model");

            $res_str = "";
            if ($this->is_super_man() || $this->is_manager()) {

                $result = $ci->team_model->find_all_active();
                for ($i = 0; $i < count($result); $i++) {
                    $id = $result[$i]["id"];
                    $name = $result[$i]["name"];
                    $res_str .= '<option value="' . $id . '" > ' . $name . ' </option> \n';

                }
                return $res_str;
            } else if ($this->is_supervisor()) {
                $result = $ci->team_model->get_by_field(array('department_id' => $ci->session->userdata("department_id")));

                for ($i = 0; $i < count($result); $i++) {
                    $id = $result[$i]["id"];
                    $name = $result[$i]["name"];
                    $res_str .= '<option value="' . $id . '" > ' . $name . ' </option> \n';
                }
                return $res_str;
            }
        }


        public function make_owning_department_combobox()
        {
            $ci = get_instance();
            $ci->load->model("department_model");

            $res_str = "";
            if ($this->is_super_man() || $this->is_manager()) {

                $result = $ci->department_model->find_all_active();
                for ($i = 0; $i < count($result); $i++) {
                    $id = $result[$i]["id"];
                    $name = $result[$i]["name"];
                    $res_str .= '<option value="' . $id . '" > ' . $name . ' </option> \n';

                }

            }
            return $res_str;
        }


        /**
         * get owning user id
         *   Super Manager can see all users except super manager
         *   Manager can see all users except super manager, manager
         *   Supervisor can see users in his owning department(s)
         *   Leader can see users in his owning room(s)
         *   Agent can see only himself
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @return string sql string putting in (...) of sql string
         */
        public function get_all_owning_user_id()
        {
            $ci = get_instance();

            $res_str = "";
            if ($this->is_super_man()) {
                return " select id from users where level != '" . $this->get_superman_type() . "' ";
            } else if ($this->is_manager()) {
                return " select id from users where level not in ('" . $this->get_superman_type() . "', '" . $this->get_manager_type() . "') ";
            } else if ($this->is_supervisor()) {
                return " select id from users where department_id = " . $ci->session->userdata("department_id") . " and level not in ('" . $this->get_superman_type() . "', '" . $this->get_manager_type() . "', '" . $this->get_supervisor_type() . "') ";
            } else if ($this->is_leader()) {
                return " select ut.user_id from users u left join users_team ut on ut.user_id = u.id where ut.team_id in (" . implode(",", $ci->session->userdata("sess_team_ids")) . ") and u.level not in ('" . $this->get_superman_type() . "', '" . $this->get_manager_type() . "', '" . $this->get_supervisor_type() . "', '" . $this->get_leader_type() . "') ";
            } else
                return $ci->session->userdata('id');
        }


        /**
         * check whether login user has authorite on given section
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    string p_section_name : name of section
         * @return boolean true if authorite, otherwise false
         */
        public function has_authorite_section($p_section_name)
        {
            if ($this->is_super_man()) {
                return true;
            }

            $ci = get_instance();
            $owned_url = $ci->session->userdata('sess_owned_url');

            if (is_array($owned_url) && in_array($p_section_name, $owned_url)) {
                return true;
            }
            return false;
        }

        /**
         * check whether login user has authorite on given module
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    array p_module_arr : names of module
         * @return boolean true if authorite, otherwise false
         */
        public function has_authorite_module($p_module_arr)
        {
            if ($this->is_super_man()) {
                return true;
            }

            $ci = get_instance();
            $owned_url = $ci->session->userdata('sess_owned_url');

            if (is_array($owned_url)) {
                foreach ($p_module_arr as $key) {
                    if (array_key_exists($key, $owned_url))
                        return true;
                }
            }
            return false;
        }

        /**
         * check whether login user has authorite on given url
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    string p_url : url address
         * @return boolean true if authorite, otherwise false
         */
        public function has_authorite_url($p_url)
        {
            if ($this->is_super_man() || $this->is_manager()) {
                return true;
            }

            $ci = get_instance();
            $owned_url = $ci->session->userdata('sess_owned_url');

            if (is_array($owned_url) && array_key_exists($p_url, $owned_url)) {
                return true;
            }
            return false;
        }

        /*
         * make combobox of owning user
         *
         * @author Dung Tao
         * @last updated             08 May 2016
         * @param    integer department_id : id of department
         * @param    integer room_id : id of room
         * @param    array agent_ids : ids of selected users
         * @return string HTML code for putting between the tag "<select></select>" to render an HTML combobox
         */

        public function make_owning_user_combobox($department_id = -1, $team_id = -1, $agent_ids = array())
        {
            $ci = get_instance();

            if ($this->is_agent()) {
                return "";
            } else if ($this->is_leader()) {
                $where_str = " u.id = ut.user_id and u.status = '" . ACTIVE . "' and ut.team_id in (" . $this->get_owning_team_ids() . ") ";
                if ($department_id > 0) {
                    $where_str .= " and u.department_id = $department_id ";
                }
                if ($team_id > 0) {
                    $where_str .= " and ut.team_id = $team_id ";
                }
                $query = $ci->db->query(" select u.* from users u, users_team ut where $where_str order by u.name asc group by u.id ");
                $result = $query->result_array();

            } else if (self::is_supervisor()) {
                $where_str = " u.status = '" . ACTIVE . "' and u.department_id = " . $ci->session->userdata("department_id");
                /*if ($department_id > 0) {
                    $where_str .= " and ud.department_id = $department_id ";
                }*/
                if ($team_id > 0) {
                    $where_str .= " and u.id in (select ut.user_id from users_team ut where ur.team_id = $team_id) ";
                }
                $query = $ci->db->query(" select u.* from users u where $where_str order by u.name asc");
                $result = $query->result_array();
            } else {
                $where_str = " u.status = '" . ACTIVE . "' ";
                if ($department_id > 0) {
                    $where_str .= " and u.department_id = $department_id ";
                }
                if ($team_id > 0) {
                    $where_str .= " and u.id in (select ut.user_id from users_team ut where ut.team_id = $team_id) ";
                }
                $query = $ci->db->query(" select u.* from users u where $where_str order by u.name asc ");
                $result = $query->result_array();
            }

            $res_str = "";
            $i = 0;
            for ($i = 0; $i < count($result); $i++) {
                $id = $result[$i]["id"];
                $name = $result[$i]["name"];
                $selected_str = is_array($agent_ids) && in_array($id, $agent_ids) ? "selected" : "";
                $res_str .= '<option value="' . $id . '" ' . $selected_str . '> ' . $name . ' </option> \n';
            }
            return $res_str;
        }

        /*
         * make combobox options of owning user
         *
         * @author Dung Tao
         * @last updated             29 Mar 2016
         * @param    integer department_id : id of department
         * @param    integer room_id : id of room
         * @param    array agent_ids : ids of selected users
         * @return array options of user
         */

        public function make_owning_user_options($department_id = -1, $team_id = -1, $agent_ids = array())
        {
            $ci = get_instance();

            if ($this->is_agent()) {
                return "";
            } else if ($this->is_leader()) {
                $where_str = " u.id = ut.user_id and u.status = '" . ACTIVE . "' and ut.team_id in (" . $this->get_owning_team_ids() . ") ";
                if ($department_id > 0) {
                    $where_str .= " and u.department_id = $department_id ";
                }
                if ($team_id > 0) {
                    $where_str .= " and ut.team_id = $team_id ";
                }
                $query = $ci->db->query(" select u.* from users u, users_team ut where $where_str  group by u.id  order by u.name asc");
                $result = $query->result_array();

            } else if (self::is_supervisor()) {
                $where_str = " u.status = '" . ACTIVE . "' and u.department_id = " . $ci->session->userdata("department_id");
                /*if ($department_id > 0) {
                    $where_str .= " and ud.department_id = $department_id ";
                }*/
                if ($team_id > 0) {
                    $where_str .= " and u.id in (select ut.user_id from users_team ut where ur.team_id = $team_id) ";
                }
                $query = $ci->db->query(" select u.* from users u where $where_str order by u.name asc");
                $result = $query->result_array();
            } else {
                $where_str = " u.status = '" . ACTIVE . "' ";
                if ($department_id > 0) {
                    $where_str .= " and u.department_id = $department_id ";
                }
                if ($team_id > 0) {
                    $where_str .= " and u.id in (select ut.user_id from users_team ut where ut.team_id = $team_id) ";
                }
                $query = $ci->db->query(" select u.* from users u where $where_str order by u.name asc ");
                $result = $query->result_array();
            }

            $options_arr = array();
            $i = 0;
            for ($i = 0; $i < count($result); $i++) {
                $id = $result[$i]["id"];
                $name = $result[$i]["name"];
                $options_arr[$id] = $name;
            }
            return $options_arr;
        }

        /**
         * get_owning_user_account_code
         *
         * @author Dung Tao
         * @last updated             15 Jul 2016
         * @return HTML code for putting between the tag "<select></select>" to render an HTML combobox
         */
        public function get_owning_user_account_code()
        {
            $ci = get_instance();

            if ($this->is_agent()) {
                return $ci->session->userdata('account_code');
            } else if ($this->is_leader()) {
                $query = $ci->db->query("select u.account_code from users u, users_team ut where u.id = ut.user_id and u.status = '" . ACTIVE . "' and ut.team_id in (" . $this->get_owning_team_ids() . ")");
                $result = $query->result_array();

                $account_code_arr = array();
                foreach ($result as $row) {
                    $account_code_arr[] = $row['account_code'];
                }

                return is_array($account_code_arr) ? "'" . implode("','", $account_code_arr) . "'" : "-1";
            } else if ($this->is_supervisor()) {
                $query = $ci->db->query(" select u.account_code from users u where u.status = '" . ACTIVE . "' and u.department_id = " . $ci->session->userdata('department_id'));
                $result = $query->result_array();

                $account_code_arr = array();
                foreach ($result as $row) {
                    $account_code_arr[] = $row['account_code'];
                }

                return is_array($account_code_arr) ? "'" . implode("','", $account_code_arr) . "'" : "-1";
            } else {
                $query = $ci->db->query("select u.account_code from users u where u.status = '" . ACTIVE . "' ");
                $result = $query->result_array();

                $account_code_arr = array();
                foreach ($result as $row) {
                    $account_code_arr[] = $row['account_code'];
                }
                return is_array($account_code_arr) ? "'" . implode("','", $account_code_arr) . "'" : "-1";
            }
        }


        /**
         * get_owning_user_ids_in_level
         *
         * @author Dung Tao
         * @purpose get owning user id based on u_type, if manager => return all active id, if leader => return his owning active id, if user => return his id
         * @last updated             29 Jul 2016
         * @return string of user id
         */
        public function get_owning_user_ids_in_level()
        {
            $ci = get_instance();
            $res_str = "";

            if ($this->is_agent()) {
                return $ci->session->userdata('id');
            } else if ($this->is_leader()) {
                $where_str = "u.status = '" . ACTIVE . "' and ut.team_id in (" . $this->get_owning_team_ids() . ") and (u.level < " . $this->_LEADER . " or (u.level = " . $this->_LEADER . " and u.id = " . $ci->session->userdata('id') . "))";
                return " select u.id from " . $ci->db->database . ".users u, " . $ci->db->database . ".users_team ut where u.id = ut.user_id and " . $where_str;
            } else if ($this->is_supervisor()) {
                $where_str = "u.status = '" . ACTIVE . "' and u.department_id = " . $ci->session->userdata('department_id') . " and (u.level < " . $this->_SUPERVISOR . " or (u.level = " . $this->_SUPERVISOR . " and u.id = " . $ci->session->userdata('id') . "))";
                return " select u.id from " . $ci->db->database . ".users u where $where_str";
            } else {
                return " select u.id from " . $ci->db->database . ".users u "; // where u.status = '" . ACTIVE . "'
            }
        }

        public function array_of_owning_user_uname_in_level()
        {
            $ci = get_instance();
            $uname_array = array();

            if ($this->is_agent()) {
                array_push($uname_array, $ci->session->userdata('username'));
            } else {
                if ($this->is_leader()) {
                    $sql = "select u.username from users u, users_team ut where u.id = ut.user_id and u.status = '" . ACTIVE . "' and ut.team_id in (" . $this->get_owning_team_ids() . ")";
                } else if ($this->is_supervisor()) {
                    $sql = "select u.username from users u where u.status = '" . ACTIVE . "' and u.department_id = " . $ci->session->userdata('department_id');
                } else {
                    $sql = "select u.username from users u where u.status = '" . ACTIVE . "'";
                }
                $result = $ci->db->query($sql)->result();
                if (count($result) > 0) {
                    foreach ($result as $r)
                        array_push($uname_array, $r->username);
                }
            }

            return $uname_array;
        }

        public function array_of_owning_user_uid_in_level()
        {
            $ci = get_instance();
            $uid_array = array();

            if ($this->is_agent()) {
                array_push($uid_array, $ci->session->userdata('id'));
            } else {
                if ($this->is_leader()) {
                    $sql = "select u.id from users u, users_team ut where u.id = ut.user_id and u.status = '" . ACTIVE . "' and ut.team_id in (" . $this->get_owning_team_ids() . ")";
                } else if ($this->is_supervisor()) {
                    $sql = "select u.id from users u where u.status = '" . ACTIVE . "' and u.department_id = " . $ci->session->userdata('department_id');
                } else {
                    $sql = "select u.id from users u where u.status = '" . ACTIVE . "'";
                }
                $result = $ci->db->query($sql)->result();
                if (count($result) > 0) {
                    foreach ($result as $r)
                        array_push($uid_array, $r->id);
                }
            }

            return $uid_array;
        }

    }
}