<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Repositories\DepartmentRepository;
use App\Repositories\TeamRepository;

class AjaxController extends Controller
{
    protected $department_repository;
    protected $team_repository;

    public function __construct(
        DepartmentRepository $department_repository,
        TeamRepository $team_repository
    )
    {
        $this->department_repository = $department_repository;
        $this->team_repository = $team_repository;
    }

    public function load_field_team($department_id){
        $result = $this->team_repository->getByFilter(array('active' => ACTIVE, "department_id" => $department_id));
        $res_str = "";
        for ($i = 0; $i < count($result); $i++) {
            $id = $result[$i]["id"];
            $name = $result[$i]["name"];
            $res_str .= "<option value='$id'> $name </option> \n";
        }
        echo $res_str;
    }
    
    
}
