<?php

namespace App\Http\Controllers;


class AboutController extends Controller
{
    public function __construct()
    {
    }

    public function index(){
        return view('about');
    }
}
