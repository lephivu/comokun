<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Customer as customer_tb;

class CustomerController extends Controller
{
    //
    public function customer(){

        //$customer_arr = \App\Customer::all('name');
        $customer_arr = customer_tb::all('name');
        $data =array(
            "project_name" => "TADUSOFT",
            "customer_arr" => $customer_arr
        );
        return view("mypage", $data);
    }
}
