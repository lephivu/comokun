<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\CardRepository;

class SearchController extends Controller
{
    protected $cardRepository;

    public function __construct(
        CardRepository $cardRepository
    )
    {
        $this->cardRepository = $cardRepository;
    }

    public function index(Request $request){

        $page = isset($request->page) ? $request->page : 0;

        //filter session
        $filters = $request->session()->has('search_results') ? $request->session()->pull('search_results') : array();

        $search_key = isset($request->search_key) ? $request->search_key : '';
        if($request->isMethod('post')){
            $filters['search_key'] = $search_key;
            

            $request->session()->put('search_results', $filters);
        }

        //get data
        $result = $this->cardRepository->getPaginate(10, $page, $filters);
        $cards_arr = $result->items();

        return view('search_results', compact("cards_arr", "result", 'search_key', 'filters'));
    }
}
