<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Repositories\CardRepository;
use App\Repositories\CardTypeRepository;

class CategoryController extends Controller
{
    protected $card_repository;
    protected $cardTypeRepository;

    public function __construct(
        CardRepository $card_repository,
        CardTypeRepository $cardTypeRepository
    )
    {
        $this->card_repository = $card_repository;
        $this->cardTypeRepository = $cardTypeRepository;
    }

    
    public function index($url ='', Request $request){

        $page = isset($request->page) ? $request->page : 0;

        //filter session
        $filters = $request->session()->has('category_search') ? $request->session()->pull('category_search') : array();

        if($request->isMethod('post')){
            $filters['search_key'] = isset($request->search_key) ? $request->search_key : '';
            $filters['search_category_id'] = isset($request->search_category_id) ?  $request->search_category_id : '';

            //category filters
            $filters['search_annual_fee'] = isset($request->search_annual_fee) ?  $request->search_annual_fee : '';
            $filters['search_card_type'] = isset($request->search_card_type) ?  $request->search_card_type : [];
            $filters['sortCard'] = isset($request->sortCard) ?  $request->sortCard : '';

            //home filters
            $filters['searchSalary'] = isset($request->searchSalary) ? $request->searchSalary : '';
            $filters['searchPaymentMethod'] = isset($request->searchPaymentMethod) ? $request->searchPaymentMethod : '';
            $filters['searchAccumulatedIncentive'] = isset($request->searchAccumulatedIncentive) ? $request->searchAccumulatedIncentive : '';

            $request->session()->put('category_search', $filters);
        }


        if(isset($category_obj) && $category_obj != null){
            $filters['search_category_id'] = $category_obj->id;
            $request->session()->put('category_search', $filters);
        }
        if(isset($tag_obj) && $tag_obj != null){
            $filters['search_tag_name'] = $url;
            $request->session()->put('category_search', $filters);
        }
        
        $search_price_from = 100;
        $search_price_to = 1000;
        if(!empty($filters['search_price'])){
            $price_arr = explode(",", $filters['search_price']);
            $search_price_from = (!empty($price_arr[0]) && $price_arr[0] > 0) ? $price_arr[0] : 0;
            $search_price_to = (!empty($price_arr[1]) && $price_arr[1] > 0) ? $price_arr[1] : 0;
        }

        //get data
        $result = $this->card_repository->getPaginate(10, $page, $filters);
        $cards_arr = $result->items();

        $card_relative_arr = DB::select("select id, ten_the,image from the where 1 order by rand() limit 8");

        return view('category', compact("cards_arr", "result", 'filters','search_price_from', 'search_price_to', 'card_relative_arr'));
    }


    public function card_detail($cardId){

        $card = $this->card_repository->getByFilter(array('id' => $cardId), true);
        //var_dump($card->cardTypeInfo);exit();
        if($card != null && $card->id > 0){
            return view('card_detail', compact('card'));
        }else {
            return view('errors.503');
        }

    }


}
