<?php

namespace App\Http\Controllers;
use App\Repositories\CardRepository;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    protected $card_repository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        CardRepository $card_repository
    )
    {
        $this->card_repository = $card_repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$card_arr = $this->card_repository->getByFilter(array(), false, 'updated_at', 'DESC', 5);
        $card_arr = DB::select("select id, ten_the, image, khuyen_mai_mo_the_text, han_muc_tin_dung, yeu_cau_thu_nhap, phi_thuong_nien from the where feature = 1 order by rand() limit 5");

        $card_relative_arr = DB::select("select id, ten_the,image from the where 1 order by rand() limit 8");

        return view('home', compact('card_arr', 'card_relative_arr'));
    }
}
