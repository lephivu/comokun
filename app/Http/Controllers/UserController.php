<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Socialite;
use App\Http\Requests;
use App\Repositories\CustomerRepository;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Models\Customer;



class UserController extends Controller
{

    protected $customerRepository;

    public function __construct(
        CustomerRepository $customerRepository
    )
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * xử lý dữ liệu callback từ facebook trả về
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::guard('customer')->login($authUser, true);
        return redirect($this->redirectTo);
    }


    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = Customer::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return Customer::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }


    public function authenticate(Request $request)
    {
        if (Auth::guard('customer')->attempt(['username' => $request->input('username'), 'password' => $request->input('password'), 'status' => ACTIVE], $request->input('remmeber'))) {
            // Authentication passed...
            $request->session()->pull("username");
            return "success";
        } else {
            $errorMsg = 'Đăng nhập không thành công';
            return view('partials/login_form', compact('errorMsg'));
        }
    }
    
    public function register(Request $request){
        $customer = array();
        $customer['name'] = $request->fullname;
        $customer['email'] = $request->email;
        $customer['mobile_phone'] = $request->phone;
        $customer['username'] = $request->username;
        $customer['gender'] = $request->gender;
        $customer['password'] = bcrypt($request->password);
        $customer['created_at'] = date('Y-m-d H:i:s');
        $customer['status'] = ACTIVE;
        $result = $this->customerRepository->insert($customer);

        if($result){
            $successMsg = 'Đăng ký thành công';
            return view('partials/register_form', compact('successMsg'));
        } else {
            $errorMsg = 'Đăng ký thất bại';
            return view('partials/register_form', compact('errorMsg', 'customer'));
        }

    }

    public function logout(Request $request){
        \Illuminate\Support\Facades\Auth::logout();
        $request->session()->flush();
        return redirect()->intended('/');

    }


}
