<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class ComparisonController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $compareArr = session('cardComparison');

        if(is_array($compareArr) && count($compareArr) > 0){
            $card_arr = DB::select("select the.*, loai_the.ten_loai as ten_loai from the left join loai_the on the.loai_the_id = loai_the.id where the.id in (".implode(',', $compareArr).") limit 4");
        } else {
            $card_arr = array();
        }

        return view('comparison', compact('card_arr'));
    }

    public function add_card($cardId, Request $request){
        //filter session
        $compareArr = session('cardComparison');

        if(!empty($cardId) && $cardId > 0 && is_array($compareArr) && !in_array($cardId, $compareArr) && count($compareArr) < 4){
            $compareArr[] = $cardId;
        }

        if(empty($compareArr) || !is_array($compareArr)){
            $compareArr = [];
        }

        $request->session()->put('cardComparison', $compareArr);

        if(count($compareArr) > 0){
            $card_arr = DB::select("select id, ten_the, feature, image from the where id in (".implode(',', $compareArr).") limit 4");
        } else {
            $card_arr = [];
        }

        return view('partials/comparison_modal', compact('card_arr'));
    }

    public function remove_card($cardId, Request $request){
        //filter session
        $compareArr = session('cardComparison');

        if(!empty($cardId) && $cardId > 0 && is_array($compareArr) && in_array($cardId, $compareArr)){
            $removeIndex = array_search($cardId, $compareArr);
            unset($compareArr[$removeIndex]);
        }

        if(empty($compareArr) || !is_array($compareArr)){
            $compareArr = [];
        }
        //session('cardComparison', $compareArr);
        $request->session()->put('cardComparison', $compareArr);

        if(count($compareArr) > 0){
            $card_arr = DB::select("select id, ten_the, feature, image from the where id in (".implode(',', $compareArr).") limit 4");
        } else {
            $card_arr = array();
        }

        return view('partials/comparison_modal', compact('card_arr'));
    }


}
