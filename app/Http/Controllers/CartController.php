<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\Item as item_model;
use Illuminate\Support\Facades\Mail;
use App\Repositories\ItemRepository;
use App\Repositories\ConfigRepository;


class CartController extends Controller
{
    protected $item_repository;
    protected $config_repository;

    public function __construct(
        ItemRepository $item_repository,
        ConfigRepository $config_repository
    )
    {
        $this->item_repository = $item_repository;
        $this->config_repository = $config_repository;
    }


    public function index(Request $request)
    {
        $config_arr = $this->config_repository->getAll();
        $config_data_arr = [];
        foreach($config_arr as $config){
            $config_data_arr[$config->key] = json_decode($config->value);
        }


        $title = 'Giỏ hàng';
        $success_mes = '';
        $cart_details_arr = is_array(session('cart')) ? session('cart') : array();
        
        $act= $request->input('act');
        if(!empty($act) ){
            if($act == 'save'){
                $cart_data = array();
                $cart_data['cus_name'] = $request->input('fullname');
                $cart_data['cus_phone'] = $request->input('phone');
                $cart_data['cus_email'] = $request->input('email');
                $cart_data['cus_address'] = $request->input('address');
                $cart_data['note'] = $request->input('note');
                $cart_data['created_at'] = date('Y-m-d H:i:s');
                $cart_id = DB::table('carts')->insertGetId($cart_data);


                $cart_details_data_arr = array();

                $cart_item_ids_arr = $request->input('cart_item_ids');
                $cart_price_arr = $request->input('cart_price');
                $cart_qty_arr = $request->input('cart_qty');
                if($cart_id > 0 && is_array($cart_item_ids_arr) && is_array($cart_qty_arr) && is_array($cart_price_arr)){
                    $total_due = 0;
                    for($i=0; $i<count($cart_item_ids_arr); $i++){
                        if(isset($cart_qty_arr[$i]) && $cart_qty_arr[$i] > 0 ){
                            $cart_detail_data = array();
                            $cart_detail_data['cart_id'] = $cart_id;
                            $cart_detail_data['item_id'] = $cart_item_ids_arr[$i];
                            $cart_detail_data['item_quantity'] = $cart_qty_arr[$i];
                            $cart_detail_data['item_price'] = $cart_price_arr[$i];
                            $cart_detail_data['amount'] = $cart_detail_data['item_price'] * $cart_detail_data['item_quantity'];

                            $email_cart_details = $cart_detail_data;
                            $email_cart_details['item_detail'] = $this->item_repository->getById($cart_detail_data['item_id']);
                            $cart_details_data_arr[] = $email_cart_details;

                            $total_due += $cart_detail_data['amount'];
                            $cart_detail_id = DB::table('cart_details')->insertGetId($cart_detail_data);
                        }
                    }

                    DB::table('carts')->where('id', $cart_id)->update(array('total_due' => $total_due));


                    //order successfully
                    session(['cart'=>array()]);
                    $success_mes = "Đã gửi đơn hàng thành công";

                    $email_subject = "Đơn Hàng Mới - Mã Đơn Hàng ".$cart_id;
                    Mail::send('email_templates.shopping_cart', array('cart_id' => $cart_id, 'total_due' => $total_due, 'cart' => $cart_data, 'cart_details' => $cart_details_data_arr), function($m) use ($email_subject){
                        $m->from('dodaxin@gmail.com', 'XachTayXin');
                        $m->to('duchoa.klink@gmail.com', 'Đơn hàng XachTayXin')->subject($email_subject);
                    });
                }
            }

            if($act == 'update'){ //update cart
                $cart_item_ids_arr = $request->input('cart_item_ids');
                $cart_price_arr = $request->input('cart_price');
                $cart_qty_arr = $request->input('cart_qty');
                $session_shopping_cart = array();
                if(is_array($cart_item_ids_arr) && is_array($cart_qty_arr) && is_array($cart_price_arr)){
                    for($i=0; $i<count($cart_item_ids_arr); $i++){
                        if(isset($cart_qty_arr[$i]) && $cart_qty_arr[$i] > 0 && $cart_item_ids_arr[$i] > 0 ){
                            $item_obj = $this->item_repository->getById($cart_item_ids_arr[$i]);

                            $new_cart = array();
                            $new_cart['item_id'] = $cart_item_ids_arr[$i];
                            $new_cart['item_name'] = $item_obj->name;
                            $new_cart['item_url'] = $item_obj->name_url;
                            //capacity : thể tích
                            $new_cart['item_capacity'] = $item_obj->capacity;
                            $new_cart['item_code'] = $item_obj->code;
                            $new_cart['quantity'] = $cart_qty_arr[$i];
                            $new_cart['price'] = $item_obj->unit_price;
                            $new_cart['img'] = $item_obj->image_file_name;
                            $session_shopping_cart[] = $new_cart;
                        }
                    }
                }
                session(['cart' => $session_shopping_cart]);
                return redirect('gio-hang');
            }
        }



        return view('cart', compact('title', 'cart_details_arr', 'success_mes', 'config_data_arr'));
    }

//    public function test_send_email(){
//        Mail::send('email_templates.shopping_cart', array(), function($m){
//            $m->from('dodaxin@gmail.com', 'Đồ Da Xịn');
//            $m->to('taomanhdung@gmail.com', 'Tào Mạnh Dũng')->subject('New Order');
//        });
//    }

}
?>