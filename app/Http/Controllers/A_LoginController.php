<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests;
use Validator;
use App\User;
use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\RegistersUsers;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User_queue as user_queue_model;

class A_LoginController extends Controller
{

    use ThrottlesLogins; //RegistersUsers,AuthenticatesUsers,

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';


    //
    public function index(){
        return view("login");
    }

    public function authenticate(Request $request)
    {
        if (Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password'), 'active' => 1], $request->input('remmeber'))) {
            // Authentication passed...
            $request->session()->pull("username");
            //$user_obj = Auth::user();
            return redirect()->intended('backend/a/item');
        } else {
            return redirect()->intended('backend/login');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function logout(Request $request){
        \Illuminate\Support\Facades\Auth::logout();
        $request->session()->flush();
        return redirect()->intended('backend/login');

    }
}
