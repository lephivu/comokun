<?php

namespace App\Http\Middleware;

use Closure;

class BeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->cart_act == 'remove_item' && $request->remove_item_id > 0){
            $shopping_cart = is_array(session('cart')) ? session('cart') : array();
            $update_shopping_cart = [];

            if(count($shopping_cart) > 0){
                foreach($shopping_cart as $cart_item){
                    if($cart_item['item_id'] != $request->remove_item_id){
                        $update_shopping_cart[] = $cart_item;
                    }
                }
            }
            session(['cart'=>$update_shopping_cart]);
        }

        return $next($request);
    }
}
