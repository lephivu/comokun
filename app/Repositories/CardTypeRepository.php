<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/29/17
 * Time: 6:33 PM
 */

namespace App\Repositories;

use App\Models\CardType as cardTypeModel;
use Illuminate\Support\Facades\DB;

class CardTypeRepository extends BaseRepository
{

    public function __construct(
        cardTypeModel $cardModel
    )
    {
        $this->model = $cardModel;
    }
    
}