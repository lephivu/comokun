<?php
namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Symfony\Component\Debug\Tests\Fixtures\ClassAlias;

abstract class BaseRepository
{

    /**
     * The Model instance.
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    public function __construct()
    {
    }

    public function getAll(){
        return $this->model->all();
    }

    /**
     * Get number of records.
     *
     * @return array
     */
    public function getNumber()
    {
        $total = $this->model->count();
        $new = $this->model->whereSeen(0)->count();
        return compact('total', 'new');
    }

    /**
     * Destroy a model.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        $this->getById($id)->delete();
    }

    /**
     * Get Model by id.
     *
     * @param  int $id
     * @return App\Models\Model
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Get data by filter array
     *
     * @param  array $filter
     * @return object
     * @author_by    dungtao
     */
    public function getByFilter($filter = [], $first = false, $order_by = '', $order_type = 'DESC', $limit = false)
    {
        $filter = $this->filterData($filter);
        $query = $this->model->query();

        if(!empty($order_by)){
            $query->orderBy($order_by, $order_type);
        }

        if (count($filter) > 0) {
            $query->where($filter);


            if ($first) {
                return $query->first();
            } else {
                if ($limit !== false) {
                    return $query->limit($limit)->get();
                } else {
                    return $query->get();
                }
            }
        }

        if ($first) {
            return $query->first();
        } else {
            return $query->get();
        }
    }

    /**
     * @param $data
     * @return array
     */
    function filterData($data)
    {
        $result = [];
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $result[] = [$key, $val];
            }
        }
        return $result;
    }

    /**
     * @param $key
     * @param $value
     * @return mixed
     * @author dungtao
     */
    public function findByWhere($where_clauses = array(), $first = false)
    {
        $query = $this->model->query();
        if(count($where_clauses) > 0){
            $query->where($where_clauses);
        }

        if($first){
            return $query->first();
        } else {
            return $query->get();
        }
    }

    /**
     * insert
     *
     * @param  array $inputs
     * @return App\Models\Model
     * @author_by    dungtao
     */
    public function insert($inputs)
    {
        return $this->model->insertGetId($inputs);
    }

    /**
     * @param array $inputs
     * @return mixed
     */
    public function saveObject($inputs)
    {
        $obj = new $this->model;
        foreach ($inputs as $key => $value) {
            $obj->$key = $value;
        }

        return $obj->save();
    }

    /**
     * update by id
     *
     * @param  integer  id
     * @param  array $inputs
     * @return App\Models\Model
     * @author_by    dungtao
     */
    public function updateById($id, $inputs)
    {
        return $this->model->where("id", $id)->update($inputs);
    }

    public function isExist($key, $value)
    {
        $count = $this->model->where($key, $value)->count();
        return $count > 0 ? true : false;
    }

    public function deleteByFilter($filter = array())
    {
        $filter = $this->filterData($filter);
        if (count($filter) > 0) {
            $this->model->where($filter)->delete();
        }
    }


}



