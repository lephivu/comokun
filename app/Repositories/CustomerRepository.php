<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/29/17
 * Time: 6:33 PM
 */

namespace App\Repositories;

use App\Models\Customer as customerModel;
use Illuminate\Support\Facades\DB;

class CustomerRepository extends BaseRepository
{

    public function __construct(
        customerModel $customerModel
    )
    {
        $this->model = $customerModel;
    }
    
}