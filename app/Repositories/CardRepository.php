<?php
/**
 * Created by PhpStorm.
 * User: manhdung
 * Date: 7/29/17
 * Time: 6:33 PM
 */

namespace App\Repositories;

use App\Models\Card as card_model;
use Illuminate\Support\Facades\DB;

class CardRepository extends BaseRepository
{

    public function __construct(
        card_model $card_model
    )
    {
        $this->model = $card_model;
    }


    public function getPaginate($per_page = PER_PAGE, $page_index = 0, $filter = null){
        $condition_arr = array();

        if (!empty($filter['search_key'])) {
            $condition_arr[] = "(the.ten_the like '%".$filter['search_key']."%')";
        }

        if (!empty($filter['searchSalary'])) {
            if($filter['searchSalary'] == 'less5000'){
                $condition_arr[] = "the.yeu_cau_thu_nhap <= 5000000";
            }
            if($filter['searchSalary'] == '5000to10000'){
                $condition_arr[] = "the.yeu_cau_thu_nhap >= 5000000";
                $condition_arr[] = "the.yeu_cau_thu_nhap <= 10000000";
            }
            if($filter['searchSalary'] == '10000to15000'){
                $condition_arr[] = "the.yeu_cau_thu_nhap >= 10000000";
                $condition_arr[] = "the.yeu_cau_thu_nhap <= 15000000";
            }
            if($filter['searchSalary'] == 'larger15000'){
                $condition_arr[] = "the.yeu_cau_thu_nhap >= 15000000";
            }
        }

        if (!empty($filter['searchPaymentMethod'])) {
            $condition_arr[] = "the.hinh_thuc_nhan_luong = ".$filter['searchPaymentMethod'];
        }

        if (!empty($filter['searchAccumulatedIncentive'])) {
            $condition_arr[] = "tich_luy_uu_dai.id = ".$filter['searchAccumulatedIncentive'];
        }

        if (!empty($filter['search_annual_fee'])) {
            if($filter['search_annual_fee'] == 'less250'){
                $condition_arr[] = "the.phi_thuong_nien <= 250000";
            }
            if($filter['search_annual_fee'] == '250to500'){
                $condition_arr[] = "the.phi_thuong_nien >= 250000";
                $condition_arr[] = "the.phi_thuong_nien <= 500000";
            }
            if($filter['search_annual_fee'] == '500to1000'){
                $condition_arr[] = "the.phi_thuong_nien >= 500000";
                $condition_arr[] = "the.phi_thuong_nien <= 1000000";
            }
            if($filter['search_annual_fee'] == 'larger1000'){
                $condition_arr[] = "the.phi_thuong_nien >= 1000000";
            }

        }


        if (isset($filter['search_card_type']) && count($filter['search_card_type']) > 0) {
            $condition_arr[] = "(the.loai_the_id in (".implode(",", $filter['search_card_type'])."))";
        }

        if (!empty($filter['search_price'])) {
            $price_arr = explode(",", $filter['search_price']);
            $price_from = (!empty($price_arr[0]) && $price_arr[0] > 0) ? $price_arr[0] * 1000 : 0;
            $price_to = (!empty($price_arr[1]) && $price_arr[1] > 0) ? $price_arr[1]*1000 : 0;

            $condition_arr[] = "the.phi_thuong_nien >= ".$price_from;
            $condition_arr[] = "the.phi_thuong_nien <= ".$price_to;
        }

        $where_str = implode(" and ", $condition_arr);

        if(empty($where_str)){
            $where_str = ' 1 ';
        }

        $sql_count = " select count(distinct the.id) as total "
            . " from the left join tich_luy_uu_dai_the on the.id = tich_luy_uu_dai_the.the_id "
            . " left join tich_luy_uu_dai on tich_luy_uu_dai_the.tich_luy_uu_dai_id = tich_luy_uu_dai.id "
            . " where $where_str ";
        $query_get_total = DB::select(DB::raw($sql_count));
        $total_count = $query_get_total[0]->total;

        $page_index_4_query = ($page_index == 0) ? (int)$page_index : (int)($page_index - 1);
        $limit = $page_index_4_query * $per_page;

        $order_by = 'the.created_at desc';
        if(!empty($filter['sortCard'])){
            if($filter['sortCard'] == 'annual_fee_asc'){
                $order_by = "the.phi_thuong_nien asc";
            }
            if($filter['sortCard'] == 'annual_fee_desc'){
                $order_by = "the.phi_thuong_nien desc";
            }
            if($filter['sortCard'] == 'popularity'){
                $order_by = "the.ten_the asc";
            }
            if($filter['sortCard'] == 'interest_rate_asc'){
                $order_by = "the.lai_suat asc";
            }
            if($filter['sortCard'] == 'interest_rate_desc'){
                $order_by = "the.lai_suat desc";
            }
        }

        $sql_result = " select the.* "
            . " from the left join tich_luy_uu_dai_the on the.id = tich_luy_uu_dai_the.the_id "
            . " left join tich_luy_uu_dai on tich_luy_uu_dai_the.tich_luy_uu_dai_id = tich_luy_uu_dai.id "
            . " where $where_str group by the.id order by $order_by limit $limit,$per_page ";
        $result = DB::select(DB::raw($sql_result));
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator($result, $total_count, $per_page, $page_index, array('path'=>'category' ));

        return $paginator;


//        $query = $this->model->query();
//
//        if (!empty($filter['search_key'])) {
//            $query->where('title', 'ten_the', '%' . $filter['search_key'] . '%');
//        }
//        $result_list = $query->paginate($per_page, ['*'], 'page', $page_index);
//        return $result_list;

    }

}