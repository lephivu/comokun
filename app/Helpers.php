<?php


if (!function_exists('parseFloat')) {
    function parseFloat($str)
    {
        if (preg_match("/([0-9.,-]+)/", $str, $match)) {
            if (strstr($str, ',')) {
                // A comma exists, that makes it easy, cos we assume it separates the decimal part.
                $str = str_replace(' ', '', $str);
                $str = str_replace(',', '', $str);    // Convert , to '' for floatval command

                return floatval($str);
            }
            return floatval($str);
        }
        return false;  //// No number found, return false
    }
}

if (!function_exists('in_array_field')) {
    function in_array_field($needle, $needle_field, $haystack, $strict = false)
    {
        if(count($haystack) == 0)
        {
            return false;
        }
        if ($strict) {
            foreach ($haystack as $item)
                if (isset($item->$needle_field) && $item->$needle_field === $needle)
                    return true;
        } else {
            foreach ($haystack as $item)
                if (isset($item->$needle_field) && $item->$needle_field == $needle)
                    return true;
        }
        return false;
    }
}

if (!function_exists('make_category_multiselect')) {
    function make_category_multiselect($category_ids_arr = [])
    {
        $model = App\Models\Category::class;
        $category_repository = new App\Repositories\CategoryRepository(new $model);
        $result = $category_repository->getAll();
        $res_str = "";
        $i = 0;
        for ($i = 0; $i < count($result); $i++) {
            $id = $result[$i]["id"];
            $name = $result[$i]["name"];
            if (in_array($id, $category_ids_arr))
                $select_str = "selected";
            else
                $select_str = "";
            $res_str .= "<option value='$id' $select_str> $name </option> \n";
        }
        return $res_str;
    }
}

if (!function_exists('make_category_combobox')) {
    function make_category_combobox($category_id = '')
    {
        $model = App\Models\Category::class;
        $category_repository = new App\Repositories\CategoryRepository(new $model);
        $result = $category_repository->getAll();
        $res_str = "";
        $i = 0;
        for ($i = 0; $i < count($result); $i++) {
            $id = $result[$i]["id"];
            $name = $result[$i]["name"];
            if ($id == $category_id)
                $select_str = "selected";
            else
                $select_str = "";
            $res_str .= "<option value='$id' $select_str> $name </option> \n";
        }
        return $res_str;
    }
}

if (!function_exists('get_item_category')) {
    function get_item_category()
    {
        $model = App\Models\Category::class;
        $category_repository = new App\Repositories\CategoryRepository(new $model);
        $result = $category_repository->getByFilter(array('active' => ACTIVE));
        return $result;
    }
}

if (!function_exists('make_tags_li')) {
    function make_tags_li($tags_arr = array())
    {
        $res_str = "";
        if(count($tags_arr) > 0){
            foreach($tags_arr as $tag_val){
                $res_str .= "<li> $tag_val </li> \n";
            }
        }
        return $res_str;
    }
}

if (!function_exists('getUrlFriendlyString')) {
    function getUrlFriendlyString($str)
    {
        return trim(preg_replace('/[-]{2,}/', '-', preg_replace('/[^a-z0-9]+/', '-', strtolower($str))), '-');
    }
}


if (!function_exists('make_card_type_checkbox')) {
    function make_card_type_checkbox($inputName = '', $checkedArr = [])
    {
        $model = App\Models\CardType::class;
        $cardTypeRepository = new App\Repositories\CardTypeRepository(new $model);
        $result = $cardTypeRepository->getByFilter(array('status' => ACTIVE));
        $res_str = "";
        $i = 0;
        for ($i = 0; $i < count($result); $i++) {
            $id = $result[$i]["id"];
            $name = $result[$i]["ten_loai"];
            if (in_array($id, $checkedArr))
                $select_str = "checked";
            else
                $select_str = "";
            $res_str .= "<label class=\"label\"><input type=\"checkbox\" name=\"$inputName\" value=\"$id\" class=\"checkbox text-uppercase\"  $select_str><span>$name</span></label> \n";

        }
        return $res_str;
    }
}