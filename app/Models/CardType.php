<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{

    protected $table='loai_the';
    public $timestamps = false;
}
