<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{

    protected $table='ngan_hang';
    public $timestamps = false;
}
