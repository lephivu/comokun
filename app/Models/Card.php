<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{

    protected $table='the';
    public $timestamps = false;

    public function bankInfo(){
        return $this->hasOne('App\Models\Bank', 'id', 'ngan_hang_id');
    }

    public function cardTypeInfo(){
        return $this->hasOne('App\Models\CardType', 'id', 'loai_the_id');
    }
}
