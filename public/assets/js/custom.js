$("#salary-tem option:first").text($("#salary option:selected").text());
//$('#salary').css('width', $('#salary-tem').width() + 60);
$('#salary').change(function(event) {
    $("#salary-tem").css('display', 'block');
    $("#salary-tem option:first").text($("#salary option:selected").text());
    //$('#salary').css('width', $('#salary-tem').width() + 60);
    $("#salary-tem").css('display', 'none');
});
$("#payment-temp option:first").text($("#payment option:selected").text());
//$('#payment').css('width', $('#payment-temp').width() + 60);
$('#payment').change(function(event) {
    $("#payment-temp").css('display', 'block');
    $("#payment-temp option:first").text($("#payment option:selected").text());
    //$('#payment').css('width', $('#payment-temp').width() + 60);
    $("#payment-temp").css('display', 'none');
});
$("#action-temp option:first").text($("#action option:selected").text());
//$('#action').css('width', $('#action-temp').width() + 60);
$('#action').change(function(event) {
    $("#action-temp").css('display', 'block');
    $("#action-temp option:first").text($("#action option:selected").text());
    //$('#action').css('width', $('#action-temp').width() + 60);
    $("#action-temp").css('display', 'none');
});
$("#service-salary-temp").css('display', 'none');
$('#service-salary').change(function(event) {
    $("#service-salary-temp").css('display', 'block');
    $("#service-salary-temp option:first").text($("#service-salary option:selected").text());
    $('#service-salary').css('width', $('#service-salary-temp').width() + 30);
    $("#service-salary-temp").css('display', 'none');
});
$('#brand-salary').change(function() {
    $('#brand-salary').css('color', 'transparent');
    $('div#view-brand-salary').text($('#brand-salary')[0].textContent);
    $('div#view-brand-salary').text($("option:selected", this).text());
});
$('#brand-open').change(function() {
    $('#brand-open').css('color', 'transparent');
    $('div#view-brand-open').text($('#brand-open')[0].textContent);
    $('div#view-brand-open').text($("option:selected", this).text());
});

$('.brand-slider-blog').owlCarousel({
    items: 3,
    loop: true,
    margin: 0,
    autoplay: true,
    nav: true,
    autoplayTimeout: 3000,
    responsive: {
        500: {
            items: 1
        },
        700: {
            items: 2
        },
        900: {
            items: 3
        }
    }
});
// home detail

$("#home-refresh").click(function(e) {
    e.preventDefault();
    $('#content').load(location.href + " #content>*", "");
    $("#salary-tem option:first").text($("#salary option:selected").text());
    $('#salary').css('width', $('#salary-tem').width() + 10);
    $("#payment-temp option:first").text($("#payment option:selected").text());
    $('#payment').css('width', $('#payment-temp').width() + 10);
    $("#action-temp option:first").text($("#action option:selected").text());
    $('#action').css('width', $('#action-temp').width() + 10);
});
$(".mega-menu").hover(
    function() {
        $(this).find('.megalv2').css('display', 'block');
    },
    function() {
        $(this).find('.megalv2').css('display', 'none');
    }
);


/* ===== Menu ===== */
$('.search').click(function () {
    if($('.search-btn').hasClass('fa-search')){
        $('.search-open').fadeIn(500);
        $('.search-btn').removeClass('fa-search');
        $('.search-btn').addClass('fa-times');
    } else {
        $('.search-open').fadeOut(500);
        $('.search-btn').addClass('fa-search');
        $('.search-btn').removeClass('fa-times');
    }
});