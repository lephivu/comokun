$(document).ready(function() {

    // Header - register form

    // $("#register_frm")
    //     .formValidation({
    //
    //         //framework: 'bootstrap',
    //         // err: {
    //         //     container: 'tooltip'
    //         // },
    //         // // trigger: 'blur',
    //         // icon: {
    //         //     required: 'glyphicon glyphicon-asterisk',
    //         //     valid: 'glyphicon glyphicon-ok',
    //         //     invalid: 'glyphicon glyphicon-remove',
    //         //     validating: 'glyphicon glyphicon-refresh'
    //         // },
    //         fields: {
    //             fullname: {
    //                 // The messages for this field are shown as usual
    //                 validators: {
    //                     notEmpty: {
    //                         message: 'Yêu cầu nhập Họ và Tên'
    //                     }
    //                 }
    //             }
    //         }
    //     });
    //

    $("#register_frm").ajaxForm({
        target: "#registerPanel",
        beforeSubmit: function() {
            $("#btn-register").html('<i class="fa fa-spinner fa-spin fa-1x"></i> Đang xử lý');
        },
        success: function(res) {
            if(res == 'success'){

            }
            $("#btn-register").html('Đăng ký');

        },
        error: function(){
            $("#btn-register").html('Đăng ký');
        }
    });

    $("#login_frm").ajaxForm({
        target: "#loginPanel",
        beforeSubmit: function() {
            $("#btn-login").html('<i class="fa fa-spinner fa-spin fa-1x"></i> Đang xử lý');
        },
        success: function(res) {
            if(res == 'success'){
                $("#login-modal").modal('hide');
                window.location.reload();
            }
            $("#btn-login").html('Đăng nhập');
        }
    });

    


    $('.kvSlider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });

    //	POPUP DETAIL	
    var slicked = false;
    $('.btnDetail').click(function(e) {
        e.preventDefault();
        $('.modalDetail').addClass('is-active');
        $('body').addClass('modal-open');
        if (!slicked) {
            $('.relatedSlider').slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                arrows: true,
                prevArrow: '<span class="fa fa-angle-left slick-prev"></span>',
                nextArrow: '<span class="fa fa-angle-right slick-next"></span>',
                dots: true,
                appendDots: '.relatedSliderDots',
                infinite: true,
                responsive: [{
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 481,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }
        slicked = true;
    });
    $('.btnCloseDetail').click(function(e) {
        e.preventDefault();
        $('.modalDetail').removeClass('is-active');
        $('body').removeClass('modal-open');
    });
    $(window).click(function() {
        $('.modalDetail').removeClass('is-active');
        $('body').removeClass('modal-open');
    });
    $('.wrapDetail, .btnDetail').click(function(e) {
        e.stopPropagation();
    });
    $(window).on('keydown', function(e) {
        var keycode = e.keyCode;
        if (keycode === 27) {
            $('.modalDetail').removeClass('is-active');
            $('body').removeClass('modal-open');
        }
    });

    //	TAB & SCROLL - card detail
    /*
    $("[tab].is-active>[tab-body]").slideDown(0);
    $("[tab-header-pc]").on("click", function() {
        var index = $(this).index(),
            tablist = $(this).closest("[headerlist]").next(),
            tabActive = tablist.find("[tab]").eq(index);
        $(this).siblings().removeClass("is-active");
        $(this).parent().children().eq(index).addClass("is-active");
        tablist.find("[tab]").removeClass("is-active");
        tabActive.addClass("is-active");
        tabActive.siblings().children("[tab-body]").slideUp(0);
        tabActive.children("[tab-body]").slideDown(0);

    });
    $("[tab-header-sp]").on("click", function() {
        var index = $(this).closest("[tab]").index(),
            tab = $(this).parent();
        headerActive = $(this).closest("[tab-container]").children("[headerlist]").find("[tab-header-pc]").eq(index);
        tab.siblings().removeClass("is-active");
        tab.toggleClass("is-active");
        tab.siblings().find("[tab-body]").slideUp(500);
        $(this).next().slideToggle(500);
        headerActive.siblings().removeClass("is-active");
        headerActive.toggleClass("is-active");
    });

    $(".tabBody>.inner").mCustomScrollbar({
        theme: "rounded-dots",
        mouseWheelPixels: 100,
        scrollInertia: 0
    });
    */

    $('.selectRegion').on('change', function() {
        var value = $(this).val();
        $(this).closest('.formGroup').find('.checkboxArea').removeClass('is-active');
        $(this).closest('.formGroup').find('.option' + value).addClass('is-active');
    })

    /*FILTER*/
    var slider = document.getElementById('slider');
    var rangeValue = document.getElementsByClassName('rangeValue');

    if (slider != null) {
        noUiSlider.create(slider, {
            start: 80000000,
            connect: [true, false],
            range: {
                'min': 0,
                'max': 100000000
            },
            format: wNumb({
                decimals: 0,
                thousand: '.',
                suffix: 'đ',
            })
        });

        slider.noUiSlider.on('update', function(values, index) {
            rangeValue[1].value = values[0];
            console.log(values);
        });

        if (rangeValue != null) {
            rangeValue[1].addEventListener('change', function() {
                slider.noUiSlider.set(rangeValue[1].value);
                console.log(values);
            });
        }

    }

    //	Scroll To Top
    $('a[href="#top"]').click(function(e) {
        e.preventDefault();
        $("html,body").animate({
            scrollTop: 0
        }, 500);
    });

    //	AutoHeight
    function autoHeight(li, n) {
        li.css("height", "auto");
        for (var i = 0, len = Math.ceil(li.length / n); i < len; i++) {
            var maxH = 0;
            for (var j = 0; j < n; j++) {
                maxH = (li.eq(i * n + j).innerHeight() >= maxH) ? li.eq(i * n + j).innerHeight() : maxH;
            }
            for (var k = 0; k < n; k++) {
                li.eq(i * n + k).css("height", maxH + "px");
            }
        }
    }

    // function autoHeightRes() {
    //     var wW = $(window).innerWidth();
    //     if (wW >= 768) {
    //         autoHeight($(".cardList .card .ttl"), 2);
    //     }
    //     if (wW < 768) {
    //         autoHeight($(".cardList .card .ttl"), 1);
    //         autoHeight($(".cardList.pageCategory .card .ttl"), 2);
    //     }
    //     if (wW < 481) {
    //         autoHeight($(".cardList.pageCategory .card .ttl"), 1);
    //     }
    // }

    // autoHeightRes();

    // $(window).resize(function() {
    //     autoHeightRes();
    // });


    $('.gridByRow,.gridByColumn').click(function() {
        $(this).addClass('is-active');
        $(this).siblings().removeClass('is-active');
    });

    $('.likeBtn').click(function() {
        $(this).toggleClass('is-active');
    });


    //	REGISTER MODAL
    $('.register-button').click(function(e) {
        e.preventDefault();
        $('#login-modal').modal('hide');
        $('#register-modal').modal('show');
    });

    $('.btnSearch01').click(function(e) {
        e.preventDefault();
        $('.formSearch').addClass('is-active');
    });

    $('.btnCloseSearch').click(function() {
        $('.formSearch').removeClass('is-active');
    });


    //	VAN 180305
    $('.close_compaCard').click(function() {
        $(this).closest('.comparison_li li .comparison_content').hide();
    });
    $('.switch > input').click(function() {
        $(this).closest('.blockLi_title').next('.table_area').toggleClass('dp_hide');
    });


    //	CHATBOX
    $('.content_chat').hide();
    $('.miz_chat02').click(function() {
        $('.content_chat').toggle(100);
    });


    //information
    $('.infoSlides').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        appendDots: $('.arrow_info')
    });
    // $('.mega-menu').click(function(e) {
    //     e.preventDefault();
    //     e.stopPropagation();
    //     $(this).siblings().find('.megalv2').hide();
    //     $(this).find('.megalv2').toggle();
    // });
    $(window).on('click', function(e) {
        var notForcus = e.target.closest('.main-menu');
        if (notForcus === null) {
            $('.megalv2').hide();
        }
    });


    //	INDEX
    $('.awardArea').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        prevArrow: '<button type="button" class="cus_prev"></button>',
        nextArrow: '<button type="button" class="cus_next"></button>',
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.partner').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        prevArrow: '<button type="button" class="arrPrev"></button>',
        nextArrow: '<button type="button" class="arrNext"></button>',
        infinite: true,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    function msieversion() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return version number
        {
            $('.bubble').width($('.message').width() + 160);
        }
        return false;
    }
    msieversion();

    // 30032018 - login
    $('.login-button').click(function() {
        $('.tabHeader.signin').removeClass('is-active');
        $('.tabHeader.login').addClass('is-active');
        $('.modalHeader.signin').removeClass('is-active');
        $('.modalHeader.login').addClass('is-active');
        $('.tabList>li.signin').removeClass('is-active');
        $('.tabList>li.login').addClass('is-active');
    });

    $('.register-button').click(function() {
        $('.tabHeader.login').removeClass('is-active');
        $('.tabHeader.signin').addClass('is-active');
        $('.modalHeader.login').removeClass('is-active');
        $('.modalHeader.signin').addClass('is-active');
        $('.tabList>li.login').removeClass('is-active');
        $('.tabList>li.signin').addClass('is-active');
    });

    $('.login').click(function() {
        $('.tabHeader.signin').removeClass('is-active');
        $('.tabHeader.login').addClass('is-active');
        $('.modalHeader.signin').removeClass('is-active');
        $('.modalHeader.login').addClass('is-active');
    });

    $('.signin').click(function() {
        $('.tabHeader.login').removeClass('is-active');
        $('.tabHeader.signin').addClass('is-active');
        $('.modalHeader.login').removeClass('is-active');
        $('.modalHeader.signin').addClass('is-active');
    });

    $('.tabHeader').click(function() {
        $('.modalHeader').toggleClass('is-active');
        $('.tabList>li').toggleClass('is-active');
    });

});