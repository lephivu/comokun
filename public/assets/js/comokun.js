/**
 * Created by manhdung on 4/8/18.
 */
var comokunLib = {

    a: false,
    // ajaxLoading: function (message) {
    //     var self = this;
    //     if (typeof message === 'undefined') {
    //         message = 'Đang xử lí...';
    //     }
    //     var template = '<i class="fa fa-spinner fa-pulse fa-2x" style="position: absolute; left: 10px; top: 13px"></i> ' + message;
    //     // jQuery("body").append('<div class="ajax-loading-overlay"></div>');
    //
    //     self.a = alertify.notify(template, 'warning', 0);
    //     return self.a;
    // },

    alertSuccess: function (message) {
        var self = this;
        if (typeof message === 'undefined') {
            message = 'Đã xong...';
        }
        alertify.success(message);
        jQuery(".ajax-loading-overlay").remove();

        if (self.a) {
            self.a.dismiss();
        }
    },

    alertError: function (message) {
        var self = this;
        if (typeof message === 'undefined') {
            message = 'Đã xong...';
        }
        alertify.error(message);
        jQuery(".ajax-loading-overlay").remove();

        if (self.a) {
            self.a.dismiss();
        }
    },

    success: function () {
        var self = this;
        jQuery(".ajax-loading-overlay").remove();

        if (self.a) {
            self.a.dismiss();
        }
    },
}