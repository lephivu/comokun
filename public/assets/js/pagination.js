// pagination promotion
$(function() {
    var pageParts = $(".promotion-page .item-promotion-blog");

    // How many parts do we have?
    var numPages = pageParts.length;
    // How many parts do we want per page?
    var perPage = 9;

    // When the document loads we're on page 1
    // So to start with... hide everything else
    pageParts.slice(perPage).hide();
    // Apply simplePagination to our placeholder
    $(".promotion-page #pagination").pagination({
        items: numPages,
        itemsOnPage: perPage,
        prevText: "<img src='./images/promotion/prev.png'>",
        nextText: "<img src='./images/promotion/next.png'>",
        cssStyle: "light-theme",
        onPageClick: function(pageNum) {
            // Which page parts do we show?
            var start = perPage * (pageNum - 1);
            var end = start + perPage;

            // First hide all page parts
            // Then show those just for our page
            pageParts.hide()
                .slice(start, end).show();
            $('html,body').animate({
                scrollTop: 300
            }, 500);
        }
    });
});
$(function() {
    var cate_page = $(".category-page .pageCategory .card");

    // How many parts do we have?
    var num_cate_page = cate_page.length;
    // How many parts do we want per page?
    var per_cate = 6;

    // When the document loads we're on page 1
    // So to start with... hide everything else
    cate_page.slice(per_cate).hide();
    // Apply simplePagination to our placeholder
    $(".category-page #pagination").pagination({
        items: num_cate_page,
        itemsOnPage: per_cate,
        prevText: "<img src='./images/promotion/prev.png'>",
        nextText: "<img src='./images/promotion/next.png'>",
        cssStyle: "light-theme",
        onPageClick: function(pageNum) {
            // Which page parts do we show?
            var start = per_cate * (pageNum - 1);
            var end = start + per_cate;

            // First hide all page parts
            // Then show those just for our page
            cate_page.hide()
                .slice(start, end).show();
            $('html,body').animate({
                scrollTop: 300
            }, 500);
        }
    });
});