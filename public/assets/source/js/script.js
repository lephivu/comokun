$(document).ready(function(){
	$('.kvSlider').slick({
		slideToShow: 1,
		slideToScroll: 1,
		arrows: false
	});
	
	/*FILTER*/
	var slider = document.getElementById('slider');
	var rangeValue = document.getElementsByClassName('rangeValue');

	noUiSlider.create(slider, {
		start: 80000000,
		connect: [true, false],
		range: {
		  'min': 0,
		  'max': 100000000
		},
		format: wNumb({
			decimals: 0,
			thousand: '.',
			suffix: 'đ',
		})
	});
	
	slider.noUiSlider.on('update', function(values, index) {
		rangeValue[1].value = values[0];
		console.log(values);
	});

	rangeValue[1].addEventListener('change', function(){
		slider.noUiSlider.set(rangeValue[1].value);
		console.log(values);
	});
	
	//	Scroll To Top
	$('a[href="#top"]').click(function(e){
	   	e.preventDefault();
    	$("html,body").animate({
			scrollTop: 0
		},500);
  	}); 
	
	//	AutoHeight
	function autoHeight(li,n){
	  li.css("height","auto");
		for(var i=0,len=Math.ceil(li.length/n); i<len; i++){
		  var maxH = 0;
		  for(var j=0; j<n; j++){
			maxH = (li.eq(i*n+j).innerHeight() >= maxH)?li.eq(i*n+j).innerHeight():maxH;
		  }
		  for(var k=0; k<n; k++){
			li.eq(i*n+k).css("height",maxH+"px");
		  }
		}
	}

	function autoHeightRes(){
	  var wW = $(window).innerWidth();
	  if(wW >= 768){
		 autoHeight($(".cardList .card .ttl"),2);
	  }
	  if(wW < 768){
		 autoHeight($(".cardList .card .ttl"),1);
	  }
	}

	autoHeightRes();

	$(window).resize(function(){
	  autoHeightRes();
	});

});