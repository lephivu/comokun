<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'before'], function() {

    Route::get('/', 'HomeController@index');


    Route::any('result', 'SearchController@index')->name('result');

    Route::get("san-pham/{item_url}", "ProductController@product_detail");
    Route::post("san-pham/{item_url}", "ProductController@product_detail");

    Route::get("about", "AboutController@index");
    Route::get("promotion", "PromotionController@index");
    Route::get("blog", "BlogController@index");
    
    Route::post("login", "UserController@authenticate")->name('login');
    Route::post("register", "UserController@register")->name('register');
    Route::get("logout", "UserController@logout")->name('logout');
    
    
    Route::get("redirectToProvider/{provider}", "UserController@redirectToProvider");
    Route::get("handleProviderCallback/{provider}", "UserController@handleProviderCallback");


    Route::get("loai/{url}", "ProductController@index");
    Route::get("nhan/{url}", "ProductController@index");

    Route::any('category', 'CategoryController@index')->name('category');
    Route::get("card/{cardId}", "CategoryController@card_detail");

    Route::get("contact", "ContactController@index");

    Route::get("comparison", "ComparisonController@index");
    Route::get("comparison_add/{cardId}", "ComparisonController@add_card");
    Route::get("comparison_remove/{cardId}", "ComparisonController@remove_card");

    Route::get("blogs/{url}", "BlogController@index");
    Route::post('blogs', 'BlogController@index')->name('blogs');

    Route::get("tin-tuc", "NewsController@index");
    Route::post("tin-tuc", "NewsController@index")->name('tin-tuc');

    Route::get("gio-hang", "CartController@index");
    Route::post("gio-hang", "CartController@index");


    Route::get("{page_url}", "PageController@detail");

});

$this->get('backend/login', 'A_LoginController@index');
$this->post('backend/login', 'Auth\AuthController@login');
$this->post('backend/login', 'A_LoginController@authenticate');
$this->get('backend/logout', 'A_LoginController@logout');

/***************    Admin routes  **********************************/
/*
Route::group(['prefix' => 'backend/a', 'middleware' => 'auth'], function() {

    // Department
    Route::resource('department', 'Backend\A_DepartmentController', ['as' => 'backend.a']);
    Route::get('department/delete/{id}', 'Backend\A_DepartmentController@delete')->name('backend.a.department.delete-department')->where(['id' => '[0-9]+']);

    // Team
    Route::resource('team', 'Backend\A_teamController', ['as' => 'backend.a']);
    Route::get('team/delete/{id}', 'Backend\A_TeamController@delete')->name('backend.a.team.delete-team')->where(['id' => '[0-9]+']);

    //User
    Route::resource('user', 'Backend\A_UserController', ['as' => 'backend.a']);
    Route::get('user/delete/{id}', 'Backend\A_UserController@delete')->name('backend.a.user.delete-user')->where(['id' => '[0-9]+']);
    Route::get('user/profile', 'Backend\A_UserController@profile')->name('backend.a.user.profile');

    // Contact Group
    Route::resource('contact_group', 'Backend\A_ContactGroupController', ['as' => 'backend.a']);
    Route::get('contact_group/delete/{id}', 'Backend\A_ContactGroupController@delete')->name('backend.a.contact_group.delete-contact-group')->where(['id' => '[0-9]+']);

    // Contact
    Route::resource('contact', 'Backend\A_ContactController', ['as' => 'backend.a']);
    Route::get('contact/delete/{id}', 'Backend\A_ContactController@delete')->name('backend.a.contact.delete-contact')->where(['id' => '[0-9]+']);

    // Category
    Route::resource('category', 'Backend\A_CategoryController', ['as' => 'backend.a']);
    Route::get('category/delete/{id}', 'Backend\A_CategoryController@delete')->name('backend.a.category.delete-category')->where(['id' => '[0-9]+']);

    // Feedback
    Route::resource('feedback', 'Backend\A_FeedbackController', ['as' => 'backend.a']);
    Route::get('feedback/delete/{id}', 'Backend\A_FeedbackController@delete')->name('backend.a.feedback.delete-feedback')->where(['id' => '[0-9]+']);

    // Item
    Route::any('item/search', 'Backend\A_ItemController@getList')->name('backend.a.item.search');
    Route::resource('item', 'Backend\A_ItemController', ['as' => 'backend.a']);
    Route::get('item/delete/{id}', 'Backend\A_ItemController@delete')->name('backend.a.item.delete-item')->where(['id' => '[0-9]+']);

    // Tag
    Route::resource('tag', 'Backend\A_TagController', ['as' => 'backend.a']);
    Route::get('tag/delete/{id}', 'Backend\A_TagController@delete')->name('backend.a.tag.delete-blog')->where(['id' => '[0-9]+']);

    // Menu
    Route::resource('menu', 'Backend\A_MenuController', ['as' => 'backend.a']);
    Route::get('menu/delete/{id}', 'Backend\A_MenuController@delete')->name('backend.a.menu.delete-item')->where(['id' => '[0-9]+']);
    Route::post('menu/create_sub_node', 'Backend\A_MenuController@createSubNode')->name('backend.a.menu.create-sub-node');
    Route::post('menu/change_status', 'Backend\A_MenuController@changeStatus')->name('backend.a.menu.change-status');


    // Blog
    Route::resource('blog', 'Backend\A_BlogController', ['as' => 'backend.a']);
    Route::get('blog/delete/{id}', 'Backend\A_BlogController@delete')->name('backend.a.blog.delete-blog')->where(['id' => '[0-9]+']);

    // Page
    Route::any('page/search', 'Backend\A_PageController@getList')->name('backend.a.page.search');
    Route::resource('page', 'Backend\A_PageController', ['as' => 'backend.a']);
    Route::get('page/delete/{id}', 'Backend\A_PageController@delete')->name('backend.a.page.delete-page')->where(['id' => '[0-9]+']);

    // Website general config
    Route::get('web_general_config/ajax_general_info', 'Backend\A_WebsiteGeneralConfigController@ajax_general_info')->name('backend.a.web_general_config.ajax_general_info');
    Route::any('web_general_config/ajax_google_analysis', 'Backend\A_WebsiteGeneralConfigController@ajax_google_analysis')->name('backend.a.web_general_config.ajax_google_analysis');
    Route::resource('web_general_config', 'Backend\A_WebsiteGeneralConfigController', ['as' => 'backend.a']);

    // Website interface config
    Route::any('web_interface_config/ajax_general_setting', 'Backend\A_WebsiteInterfaceConfigController@ajax_general_setting')->name('backend.a.web_interface_config.ajax_general_setting');
    Route::any('web_interface_config/ajax_home_page', 'Backend\A_WebsiteInterfaceConfigController@ajax_home_page')->name('backend.a.web_interface_config.ajax_home_page');
    Route::any('web_interface_config/ajax_item_list', 'Backend\A_WebsiteInterfaceConfigController@ajax_item_list_page')->name('backend.a.web_interface_config.ajax_item_list');
    Route::any('web_interface_config/ajax_contact_page', 'Backend\A_WebsiteInterfaceConfigController@ajax_contact_page')->name('backend.a.web_interface_config.ajax_contact_page');
    Route::resource('web_interface_config', 'Backend\A_WebsiteInterfaceConfigController', ['as' => 'backend.a']);

});
Route::group(['middleware' => [ 'web']], function() {
    //ajax
    Route::get("ajax/load_field_team/{id}", "AjaxController@load_field_team")->name('ajax.load_field_team')->where(['id' => '[0-9]+']);
});
*/